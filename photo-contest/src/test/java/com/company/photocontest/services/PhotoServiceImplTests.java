package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.PhotoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.*;

import static com.company.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceImplTests {
    @Mock
    PhotoRepository mockRepository;

    @InjectMocks
    PhotoServiceImpl service;
    @Test
    public void get_Should_CallRepository() {
        // Arrange

        Mockito.when(mockRepository.getAll())
                .thenReturn(null);

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }
    @Test
    public void getById_Should_Call_Repository() {

        var mockPhoto = createMockPhoto();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPhoto);

        // Act
        service.getById(mockPhoto.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockPhoto.getId());
    }
    @Test
    public void getByTitle_Should_Call_Repository() {

        var mockPhoto = createMockPhoto();

        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(mockPhoto);

        // Act
        service.getByTitle(mockPhoto.getTitle());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByTitle(mockPhoto.getTitle());
    }


    @Test
    public void create_Should_Call_Repository_When_PhotoDoesNotExist() {
        var mockPhoto = createMockPhoto();
        var mockUser = createMockUser();
        var mockImage =
                new MockMultipartFile("image", "MockTitle.jpg",
                        "image/jpeg", "MockTitle.jpg".getBytes());

        Mockito.when(mockRepository.getByTitle(mockPhoto.getTitle()))
                .thenThrow(EntityNotFoundException.class);
        service.create(mockPhoto, mockImage, mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .create(mockPhoto);
    }


    @Test
    public void create_Throws_When_PhotoWithSameTitleExists() {
        var mockPhoto = createMockPhoto();
        var mockUser = createMockUser();
        var mockImage =
                new MockMultipartFile("image", "MockTitle.jpg",
                        "image/jpeg", "MockTitle.jpg".getBytes());

        Mockito.when(mockRepository.getByTitle(mockPhoto.getTitle()))
                .thenReturn(mockPhoto);


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockPhoto, mockImage, mockUser));
    }

    @Test
    public void update_Throw_When_PhotoWithSameTitleExists() {
        var mockPhoto = createMockPhoto();
        var anotherPhoto = createMockPhoto();
        anotherPhoto.setId(2);


        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(anotherPhoto);

        Assertions.assertThrows(DuplicateEntityException.class, () ->
                service.update(mockPhoto, "mockImage.jpg"));
    }


    @Test
    public void Update_Should_Call_Repository() {
        var mockPhoto = createMockPhoto();
        var mockPhoto2 = createMockPhoto();
        mockPhoto2.setTitle("MockTitle2");
        var mockUser = createMockUser();
        var mockImage =
                new MockMultipartFile("image", "MockTitle.jpg",
                        "image/jpeg", "MockTitle.jpg".getBytes());

        Mockito.when(mockRepository.getByTitle(mockPhoto.getTitle()))
                .thenThrow(EntityNotFoundException.class );

        service.create(mockPhoto, mockImage, mockUser);
        String oldImageName = mockPhoto.getImage();
        service.update(mockPhoto, oldImageName);

        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockPhoto);
    }

    @Test
    public void delete_Should_Call_Repository_WhenPhotoExist()
    {
        var mockPhoto = createMockPhoto();

        service.delete(mockPhoto.getId());

        Mockito.verify(mockRepository).delete(mockPhoto.getId());
    }

}
