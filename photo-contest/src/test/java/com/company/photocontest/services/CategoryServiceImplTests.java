package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Category;
import com.company.photocontest.models.Contest;
import com.company.photocontest.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.photocontest.Helpers.createMockCategory;
import static com.company.photocontest.Helpers.createMockUser;


@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll()).
                thenReturn(null);

        //Act
        service.getAll();
        //Assert
        Mockito.verify(mockRepository).getAll();
    }
    @Test
    public void getById_Should_Return_WhenCategory_MatchByIdExists() {
        // Arrange
        var mockCategory = createMockCategory();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCategory);

        // Act
        Category result = service.getById(mockCategory.getId());

        // Assert
        Assertions.assertEquals(mockCategory, result);
    }
    @Test
    public void getById_Should_CallRepository(){
        var mockCategory = createMockCategory();

        Mockito.when(mockRepository.getById(mockCategory.getId()))
                .thenReturn(null);

        service.getById(mockCategory.getId());

        Mockito.verify(mockRepository).getById(mockCategory.getId());
    }
    @Test
    public void getByName_Should_CallRepository(){
        var mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenReturn(null);

        service.getByName(mockCategory.getName());

        Mockito.verify(mockRepository).getByName(mockCategory.getName());
    }



    @Test
    public void delete_Should_Call_Repository(){
        var mockCategory = createMockCategory();

        service.delete(mockCategory.getId());
        Mockito.verify(mockRepository).delete(mockCategory.getId());

    }

}
