package com.company.photocontest.services;

import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Score;
import com.company.photocontest.repositories.contracts.ScoreRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.company.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ScoreServiceImplTests {
    @Mock
    ScoreRepository mockRepository;

    @InjectMocks
    ScoreServiceImpl service;
    @Test
    public void get_Should_CallRepository() {
        // Arrange

        Mockito.when(mockRepository.getAll())
                .thenReturn(null);

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }
    @Test
    public void getById_Should_Call_Repository() {

        var mockScore = createMockScore();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockScore);

        // Act
        service.getById(mockScore.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockScore.getId());
    }
    @Test
    public void getByJuror_Should_Call_Repository() {

        var mockScore = createMockScore();
        List<Score> scoreList = new ArrayList<>();
        scoreList.add(mockScore);
        var mockUser = createMockUser();

        Mockito.when(mockRepository.getByJuror(Mockito.any()))
                .thenReturn(scoreList);

        // Act
        service.getByJuror(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByJuror(mockUser);
    }

    @Test
    public void create_Should_Call_Repository() {
        var mockScore = createMockScore();
        var mockPhoto = createMockPhoto();
        var mockContest = createMockContest();
        var mockUser = createMockUser();
        service.create(mockScore, mockPhoto, mockContest, mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .create(mockScore);
    }
    @Test
    public void update_Should_Call_Repository() {
        var mockScore = createMockScore();

        service.update(mockScore);

        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockScore);
    }
    @Test
    public void delete_Should_Call_Repository() {
        var mockScore = createMockScore();

        service.delete(mockScore.getId());

        Mockito.verify(mockRepository,Mockito.times(1))
                .delete(mockScore.getId());
    }
}
