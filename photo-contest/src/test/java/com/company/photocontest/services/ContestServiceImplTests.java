package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Contest;
import com.company.photocontest.repositories.contracts.ContestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository mockRepository;

    @InjectMocks
    ContestServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll()).
                thenReturn(null);

        //Act
        service.getAll();
        //Assert
        Mockito.verify(mockRepository).getAll();
    }
    @Test
    public void getById_Should_Return_WhenContest_MatchByIdExists() {
        // Arrange
        Contest mockContest = createMockContest();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockContest);

        // Act
        Contest result = service.getById(mockContest.getId());

        // Assert
        Assertions.assertEquals(mockContest, result);
    }
    @Test
    public void getById_Should_CallRepository(){
        var mockContest = createMockContest();

        Mockito.when(mockRepository.getById(mockContest.getId()))
                .thenReturn(null);

        service.getById(mockContest.getId());

        Mockito.verify(mockRepository).getById(mockContest.getId());
    }
    @Test
    public void getByTittle_Should_CallRepository(){
        var mockContest = createMockContest();

        Mockito.when(mockRepository.getByTitle(mockContest.getTitle()))
                .thenReturn(null);

        service.getByTitle(mockContest.getTitle());

        Mockito.verify(mockRepository).getByTitle(mockContest.getTitle());
    }
    @Test
    public void getByCategory_Should_CallRepository(){
        var mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByCategory(mockCategory.getName()))
                .thenReturn(null);

        service.getByCategory(mockCategory.getName());

        Mockito.verify(mockRepository).getByCategory(mockCategory.getName());
    }

    @Test
    public void create_Should_Call_Repository_When_ContestDoesNotExist() {
        var mockContest = createMockContest();
        var mockUser = createMockUser();

        Mockito.when(mockRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class );

        service.create(mockContest, mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .create(mockContest);
    }


    @Test
    public void create_Throws_When_ContestWithSameTitleExists() {
        var mockContest = createMockContest();
        var mockUser = createMockUser();
        mockUser.setOrganiser(true);

        Mockito.when(mockRepository.getByTitle(mockContest.getTitle()))
                .thenReturn(mockContest);


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockContest, mockUser));
    }

    @Test
    public void update_Throw_When_ContestWithSameTitleExists() {
        Contest mockContent = createMockContest();
        Contest anotherContent = createMockContest();
        anotherContent.setId(2);


        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(anotherContent);

        Assertions.assertThrows(DuplicateEntityException.class, () ->
                service.update(mockContent));
    }


    @Test
    public void Update_Should_Call_Repository() {
        var mockContest = createMockContest();
        var mockUser = createMockUser();
        mockUser.setOrganiser(true);

        Mockito.when(mockRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class );

        service.create(mockContest, mockUser);
        service.update(mockContest);

        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void delete_Should_Call_Repository(){
        var mockContest = createMockContest();

        service.delete(mockContest.getId());
        Mockito.verify(mockRepository).delete(mockContest.getId());

    }


}
