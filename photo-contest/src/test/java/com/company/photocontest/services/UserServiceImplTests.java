package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import com.company.photocontest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static com.company.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void get_Should_CallRepository() {
        //Arrange
        UserFilterOptions mockUserFilterOptions = createMockUserFilterOptions();
        Mockito.when(mockRepository.get(mockUserFilterOptions)).
                thenReturn(null);

        //Act
        service.get(mockUserFilterOptions);
        //Assert
        Mockito.verify(mockRepository).get(mockUserFilterOptions);
    }
    @Test
    public void getById_Should_ReturnUser_When_MatchByIdExists(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getById(user.getId())).thenReturn(user);
        //Act
        User targetUser = service.getById(user.getId());
        //Assert
        Assertions.assertEquals(targetUser, user);
    }

    @Test
    public void getById_Should_Throw_When_UserDoesntExist(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getById(Mockito.anyInt())).
                thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,()-> service.getById(user.getId()));
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_MatchByUsernameExists(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        //Act
        User targetUser = service.getByUsername(user.getUsername());
        //Assert
        Assertions.assertEquals(targetUser, user);
    }

    @Test
    public void getByEmail_Should_ReturnUser_When_MatchByUsernameExists(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenReturn(user);
        //Act
        User targetUser = service.getByEmail(user.getUsername());
        //Assert
        Assertions.assertEquals(targetUser, user);
    }

    @Test
    public void create_Should_CallRepository_When_ValidUser() {
        // Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        // Act
        service.create(user);

        // Assert
        Mockito.verify(mockRepository)
                .create(user);
    }

    @Test
    public void update_Should_CallRepository_When_EmailAndUsernameAreFree(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        //Act
        service.update(user);
        //Assert
        Mockito.verify(mockRepository).update(user);
    }

    @Test
    public void update_Should_CallRepository_WhenUsernameExistsButSameId(){
        //Arrange
        User user = createMockUser();
        Mockito.when(mockRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        //Act
        service.update(user);
        //Assert
        Mockito.verify(mockRepository).update(user);
    }

    @Test
    public void update_Should_Throw_When_EmailAlreadyTaken(){
        //Arrange
        User user = createMockUser();
        user.setId(2);
        Mockito.when(mockRepository.getByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenReturn(createMockUser());
        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class, ()-> service.update(user));
    }

    @Test
    public void delete_Should_CallRepository() {
        //Arrange
        User user = createMockUser();
        //Act
        service.delete(user.getId());
        //Assert
        Mockito.verify(mockRepository).delete(user.getId());
    }

}
