package com.company.photocontest;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.models.*;

public class Helpers {
    public static Photo createMockPhoto(){
        Photo mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setTitle("MockTitle");
        mockPhoto.setStory("MockStory");
        return mockPhoto;
    }
    public static Score createMockScore(){
        Score mockScore = new Score();
        mockScore.setId(1);
        mockScore.setComment("MockComment");
        mockScore.setJuror(createMockUser());
        mockScore.setPhoto(createMockPhoto());
        mockScore.setPoints(1);
        return mockScore;
    }
    public static User createMockUser(){
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setUsername("MockUsername");
        mockUser.setEmail("mock@user.com");
        mockUser.setPassword("MockPassword");
        mockUser.setPoints(1);
        return mockUser;
    }
    public static Category createMockCategory(){
        Category mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        return mockCategory;
    }

    public static Contest createMockContest(){
        Contest mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("MockTittle");
        mockContest.setCategory(createMockCategory());
        mockContest.setPhase(Phase.PHASEONE);
        mockContest.setIsOpen(true);
        mockContest.setCoverPhoto(createMockPhoto());
        return mockContest;
    }

    public static UserFilterOptions createMockUserFilterOptions() {
        return new UserFilterOptions("username",
                "firstName",
                "lastName",
                1,
                10,
                "points",
                "desc",
                true);

    }
}
