package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import com.company.photocontest.repositories.contracts.UserRepository;
import com.company.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> get(UserFilterOptions userFilterOptions) {
        return userRepository.get(userFilterOptions);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public void create(User user) {
        boolean isDuplicateByUsername = true;
        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            isDuplicateByUsername = false;
        }
        if (isDuplicateByUsername) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        boolean isDuplicateByEmail = true;
        try {
            userRepository.getByEmail(user.getEmail());

        } catch (EntityNotFoundException e) {
            isDuplicateByEmail = false;
        }
        if (isDuplicateByEmail) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        userRepository.create(user);
    }

    @Override
    public void update(User user) {
        boolean isDuplicateByUsername = true;
        try {
            User existingUser = userRepository.getByUsername(user.getUsername());
            if  (existingUser.getId() == user.getId()) {
                isDuplicateByUsername = false;
            }
        } catch (EntityNotFoundException e) {
            isDuplicateByUsername = false;
        }
        if (isDuplicateByUsername) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        boolean isDuplicateByEmail = true;
        try {
            User existingUser = userRepository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                isDuplicateByEmail = false;
            }
        } catch (EntityNotFoundException e) {
            isDuplicateByEmail = false;
        }
        if (isDuplicateByEmail) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        userRepository.update(user);
    }

    @Override
    public void delete(int id) {
        userRepository.delete(id);
    }

    @Override
    public Page<User> findPageAddJury(int pageNumber, UserFilterOptions userFilterOptions) {
        Pageable pageable = PageRequest.of(pageNumber - 1,6);
        return userRepository.getAllAddJury(pageable, userFilterOptions);
    }
    @Override
    public Page<User> findPageInContest(int pageNumber, UserFilterOptions userFilterOptions, Contest contest) {
        Pageable pageable = PageRequest.of(pageNumber - 1,6);
        return userRepository.getAllJuryInContest(pageable, userFilterOptions, contest);
    }
}
