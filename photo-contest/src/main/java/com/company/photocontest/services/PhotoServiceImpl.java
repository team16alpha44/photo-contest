package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.PhotoFilterOptions;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.PhotoRepository;
import com.company.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private static final String rootPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";
    private static final String rootPathBuild = "photo-contest\\build\\resources\\main\\static\\photoUpload\\";

    private void init() {
        try {
            Files.createDirectories(Paths.get(rootPath));
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
        init();
    }

    @Override
    public List<Photo> getAll() {
        return photoRepository.getAll();
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public Photo getByTitle(String title) {
        return photoRepository.getByTitle(title);
    }

    @Override
    public List<Photo> getByAuthor(User author) {
        return photoRepository.getByAuthor(author);
    }

    @Override
    public Page<Photo> findPage(int pageNumber, PhotoFilterOptions photoFilterOptions) {
        Pageable pageable = PageRequest.of(pageNumber - 1,6);
        return photoRepository.getByPagination(pageable, photoFilterOptions);
    }

    @Override
    public void delete(int id) {
        try {
            photoRepository.delete(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Photo", "id", Integer.toString(id), "photoUpload folder");
        }
    }

    @Override
    public void create(Photo photo, MultipartFile image, User author) {
        boolean isDuplicateByTitle = true;
        try {
            photoRepository.getByTitle(photo.getTitle());

        } catch (EntityNotFoundException e) {
            isDuplicateByTitle = false;
        }

        if (isDuplicateByTitle) {
            throw new DuplicateEntityException("Photo", "title", photo.getTitle());
        }
        try {
            String imageName = photo.getTitle() + ".jpg";
            photo.setAuthor(author);
            Path userDir = Paths.get(rootPath + photo.getAuthor().getUsername());
            Path userDirBuild = Paths.get(rootPathBuild + photo.getAuthor().getUsername());
            photo.setImage("photoUpload/" + photo.getAuthor().getUsername() + "/" + imageName);
            Files.createDirectories(userDir);
            Files.createDirectories(userDirBuild);
            Files.copy(image.getInputStream(), userDir.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(image.getInputStream(), userDirBuild.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            photoRepository.create(photo);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    @Override
    public void createCover(Photo photo, MultipartFile image, User author) {
        boolean isDuplicateByTitle = true;
        try {
            photoRepository.getByTitle(photo.getTitle());

        } catch (EntityNotFoundException e) {
            isDuplicateByTitle = false;
        }

        if (isDuplicateByTitle) {
            throw new DuplicateEntityException("Photo", "title", photo.getTitle());
        }
        try {
            String imageName = photo.getTitle(); //+ ".jpg";
            photo.setAuthor(author);
            Path userDir = Paths.get(rootPath + photo.getAuthor().getUsername());
            Path userDirBuild = Paths.get(rootPathBuild + photo.getAuthor().getUsername());
            photo.setImage("photoUpload/" + photo.getAuthor().getUsername() + "/" + imageName);
            Files.createDirectories(userDir);
            Files.createDirectories(userDirBuild);
            Files.copy(image.getInputStream(), userDir.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(image.getInputStream(), userDirBuild.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            photoRepository.create(photo);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void update(Photo photo, String oldImageName) {
        boolean isDuplicateTitle = true;
        try {
            Photo exist = photoRepository.getByTitle(photo.getTitle());
            if (exist.equals(photo)) {
                isDuplicateTitle = false;
            }
        } catch (EntityNotFoundException e) {
            isDuplicateTitle = false;
        }
        if (isDuplicateTitle) {
            throw new DuplicateEntityException("Photo", "title", photo.getTitle());
        }
        try {
            Path userDir = Paths.get(rootPath + photo.getAuthor().getUsername());
            Path userDirBuild = Paths.get(rootPathBuild + photo.getAuthor().getUsername());
            photo.setImage("photoUpload/" + photo.getAuthor().getUsername() + "/" + photo.getTitle() + ".jpg");
            Files.move(Paths.get(userDir + oldImageName.replace('/', '\\')),
                    userDir.resolve(photo.getTitle() + ".jpg"),
                    StandardCopyOption.REPLACE_EXISTING);
            Files.move(Paths.get(userDirBuild + oldImageName.replace('/', '\\')),
                    userDirBuild.resolve(photo.getTitle() + ".jpg"),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        photoRepository.update(photo);
    }
}
