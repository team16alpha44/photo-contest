package com.company.photocontest.services.contracts;

import com.company.photocontest.models.Category;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.AbstractCRUDRepository;

import java.util.List;

public interface CategoryService  {
    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);

    void delete(int id);



    void addCategoryToContest(String category);
}
