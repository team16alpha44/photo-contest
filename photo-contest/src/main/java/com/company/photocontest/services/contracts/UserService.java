package com.company.photocontest.services.contracts;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    List<User> get(UserFilterOptions userFilterOptions);

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    void create(User user);

    void update(User user);

    void delete(int id);

    Page<User> findPageAddJury(int pageNumber, UserFilterOptions userFilterOptions);
    Page<User> findPageInContest(int pageNumber, UserFilterOptions userFilterOptions, Contest contest);

}
