package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.ProfilePictureRepository;
import com.company.photocontest.services.contracts.ProfilePictureService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class ProfilePictureServiceImpl implements ProfilePictureService {
    private static final String rootPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";
    private static final String rootPathBuild = "photo-contest\\build\\resources\\main\\static\\photoUpload\\";
    private static final String PROFILE_SUFFIX = "\\profilePictures\\";
    private final ProfilePictureRepository profilePictureRepository;

    private void init() {
        try {
            Files.createDirectories(Paths.get(rootPath));
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public ProfilePictureServiceImpl(ProfilePictureRepository profilePictureRepository) {
        this.profilePictureRepository = profilePictureRepository;
        init();
    }


    @Override
    public ProfilePicture getById(int id) {
        return profilePictureRepository.getById(id);
    }

    @Override
    public List<ProfilePicture> getAll() {
        return profilePictureRepository.getAll();
    }

    @Override
    public List<ProfilePicture> getByUser(User user) {
        return profilePictureRepository.getByUser(user);
    }


    @Override
    public void create(ProfilePicture profilePicture, MultipartFile image, User executingUser) {
        boolean isDuplicateByTitle = true;
        try {
            profilePictureRepository.getByTitle(profilePicture.getTitle());

        } catch (EntityNotFoundException e) {
            isDuplicateByTitle = false;
        }

        if (isDuplicateByTitle) {
            throw new DuplicateEntityException("Profile picture", "title", profilePicture.getTitle());
        }
        try {
            String imageName = profilePicture.getTitle() + ".jpg";
            profilePicture.setUser(executingUser);
            Path userDir = Paths.get(rootPath + profilePicture.getUser().getUsername() + PROFILE_SUFFIX);
            Path userDirBuild = Paths.get(rootPathBuild + profilePicture.getUser().getUsername() + PROFILE_SUFFIX);
            profilePicture.setImage("photoUpload/" + profilePicture.getUser().getUsername() + "/profilePictures/" + imageName);
            Files.createDirectories(userDir);
            Files.createDirectories(userDirBuild);
            Files.copy(image.getInputStream(), userDir.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(image.getInputStream(), userDirBuild.resolve(imageName), StandardCopyOption.REPLACE_EXISTING);
            profilePictureRepository.create(profilePicture);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void update(ProfilePicture profilePicture) {
        try {
            ProfilePicture current = getCurrent(profilePicture.getUser());
            if (current!=null) {
                current.setActive(false);
                profilePictureRepository.update(current);
            }
            profilePictureRepository.update(profilePicture);
        } catch (EntityNotFoundException e) {
            profilePictureRepository.update(profilePicture);
        }

    }

    @Override
    public void delete(int id) {
        try {
            profilePictureRepository.delete(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Profile picture", "id", Integer.toString(id), "profilePictures folder");
        }
    }

    public ProfilePicture getCurrent(User user){
        try {
            return profilePictureRepository.getCurrent(user);
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

}
