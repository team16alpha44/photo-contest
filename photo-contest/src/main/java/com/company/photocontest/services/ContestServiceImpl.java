package com.company.photocontest.services;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ContestMapper;
import com.company.photocontest.helpers.PhotoMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.PhotoDto;
import com.company.photocontest.repositories.contracts.*;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ContestServiceImpl implements ContestService {
    private static final String CREATE_CONTEST_ERROR_MESSAGE = "Only organiser  can create contest.";
    private final ContestRepository contestRepository;
    private final PhotoContestRelationRepository photoContestRelationRepository;
    private final ScoreRepository scoreRepository;
    private final PhotoRepository photoRepository;
    private final UserRepository userRepository;

    private static final String rootPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";
    private static final String rootPathBuild = "photo-contest\\build\\resources\\main\\static\\photoUpload\\";
    private final PhotoService photoService;
    private final PhotoMapper photoMapper;

    private void init() {
        try {
            Files.createDirectories(Paths.get(rootPath));
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, PhotoContestRelationRepository photoContestRelationRepository, ScoreRepository scoreRepository, PhotoRepository photoRepository, UserRepository userRepository, ContestMapper contestMapper, PhotoService photoService, UserService userService, PhotoMapper photoMapper, AuthenticationHelper authenticationHelper) {
        this.contestRepository = contestRepository;
        this.photoContestRelationRepository = photoContestRelationRepository;
        this.scoreRepository = scoreRepository;
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
        this.photoService = photoService;
        this.photoMapper = photoMapper;
    }

    @Override
    public List<Contest> get(ContestFilterOptions contestFilterOptions) {
        List<Contest> contests = contestRepository.get(contestFilterOptions);
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public List<Contest> getAll() {
        List<Contest> contests = contestRepository.getAll();
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public Contest getById(int id) {
        return updatePhase(contestRepository.getById(id));
    }

    @Override
    public Contest getByTitle(String title) {
        return updatePhase(contestRepository.getByTitle(title));
    }

    @Override
    public List<Contest> getByCategory(String category) {
        List<Contest> contests = contestRepository.getByCategory(category);
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public List<Contest> getByPhase(Phase phase) {
        List<Contest> contests = contestRepository.getByPhase(phase);
        contests.forEach(this::updatePhase);
        return contests;
    }

    public Photo getNumberOne(int contestId) {
        Optional<PhotoContestRelation> winner = photoContestRelationRepository.getSortedByResult(contestId).stream().findFirst();
        return photoRepository.getById(winner.orElseThrow().getPhotoId());
    }

    @Override
    public List<Contest> getByPhaseOneOpen() {
        List<Contest> contests = contestRepository.getByPhaseOneOpen();
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public Page<Contest> findPage(int pageNumber, ContestFilterOptions contestFilterOptions) {
        if (contestFilterOptions.getCategory().isEmpty()) {
            contestFilterOptions.setCategory("");
        }
        Pageable pageable = PageRequest.of(pageNumber - 1, 6);
        Page<Contest> contests = contestRepository.getByPagination(pageable, contestFilterOptions);
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public Page<Contest> findPageForParticipant(int pageNumber, ContestFilterOptions contestFilterOptions, int participantId) {
        if (contestFilterOptions.getTitle().isEmpty()) {
            contestFilterOptions.setTitle("");
        }
        if (contestFilterOptions.getCategory().isEmpty()) {
            contestFilterOptions.setCategory("");
        }
        Pageable pageable = PageRequest.of(pageNumber - 1, 6);
        Page<Contest> contests = contestRepository.getByParticipantPagination(pageable, contestFilterOptions, participantId);
        contests.forEach(this::updatePhase);
        return contests;
    }

    @Override
    public List<Contest> getByParticipant(int userId) {
        return contestRepository.getByParticipant(userId);
    }

    @Override
    public void delete(int id) {
        contestRepository.delete(id);
    }

    @Override
    public void updateContestPhoto(int contestId, MultipartFile image, User user) {
        Contest exist = contestRepository.getById(contestId);
        Photo photo = photoMapper.dtoToPhoto(new PhotoDto(image.getOriginalFilename(), ""));
        photo.setTitle(exist.getTitle());
        photo.setStory("COVER PHOTO");
        photoService.create(photo , image , user);
        Photo oldCover= exist.getCoverPhoto();
        exist.setCoverPhoto(photo);
        contestRepository.update(exist);
        if(oldCover != null){
            photoService.delete(oldCover.getId());
        }
    }

    @Override
    public void create(Contest contest, User user) {
        checkModifyPermissions(user);

        boolean isDuplicateByTitle = true;
        try {
            contestRepository.getByTitle(contest.getTitle());

        } catch (EntityNotFoundException e) {
            isDuplicateByTitle = false;
        }

        if (isDuplicateByTitle) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }

        contestRepository.create(contest);
    }

    @Override
    public void update(Contest contest) {
        boolean isDuplicateTitle = true;
        try {
            Contest exist = contestRepository.getByTitle(contest.getTitle());
            if (exist.equals(contest)) {
                isDuplicateTitle = false;
            }
        } catch (EntityNotFoundException e) {
            isDuplicateTitle = false;
        }
        if (isDuplicateTitle) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contestRepository.update(contest);
    }


    private void checkModifyPermissions(User user) {
        if (!(user.isOrganiser())) {
            throw new AuthorizationException(CREATE_CONTEST_ERROR_MESSAGE);
        }
    }

    private Contest updatePhase(Contest contest) {
        if (contest.getPhase() != Phase.FINAL) {
            if (contest.getExpiredDatePhase1().isBefore(LocalDateTime.now())) {
                contest.setPhase(Phase.PHASETWO);
                if (contest.getExpiredDatePhase2().isBefore(LocalDateTime.now())) {
                    contest.setPhase(Phase.FINAL);

                    calculateResults(contest);
                    distributePoints(contest.getId());
                }
                update(contest);
            }
        }
        return contest;
    }

    public void addPhotoToContest(int contestId, int photoId) {
        Contest contest = contestRepository.getById(contestId);
        if (contest.getPhotos().stream().anyMatch(pc -> pc.getContestId() == contestId && pc.getPhotoId() == photoId)) {
            throw new DuplicateEntityException("Contest", "photo", Integer.toString(photoId));
        }
        PhotoContestRelation photoContestRelation = new PhotoContestRelation();
        photoContestRelation.setContestId(contestId);
        photoContestRelation.setPhotoId(photoId);
        photoContestRelationRepository.create(photoContestRelation);
        addParticipationPoints(photoId, contest);
    }

    public void removePhotoFromContest(int contestId, int photoId) {
        Contest contest = contestRepository.getById(contestId);
        if (contest.getPhotos().stream().noneMatch(pc -> pc.getContestId() == contestId && pc.getPhotoId() == photoId)) {
            throw new EntityNotFoundException("Photo", photoId);
        }
        subtractParticipationPoints(photoId, contest);
        photoContestRelationRepository.delete(contestId, photoId);
    }


    private void calculateResults(Contest contest) {
        for (PhotoContestRelation submission : contest.getPhotos()) {
            submission.setResult(getTotalScoreForContest(submission.getPhotoId(), contest));
            photoContestRelationRepository.update(submission);
        }
    }

    private int getTotalScoreForContest(int photoId, Contest contest) {
        List<Score> scoresForCurrentPhoto = scoreRepository.getByContest(contest).stream()
                .filter(s -> s.getPhoto().getId() == photoId).toList();

        int totalScore = scoresForCurrentPhoto.stream().mapToInt(Score::getPoints)
                .sum();
        totalScore += (contest.getJurors().size() - scoresForCurrentPhoto.size()) * 3;
        return totalScore;
    }

    private void distributePoints(int contestId) {
        List<PhotoContestRelation> firstPlaceWinners = new ArrayList<>();
        List<PhotoContestRelation> secondPlaceWinners = new ArrayList<>();
        List<PhotoContestRelation> thirdPlaceWinners = new ArrayList<>();

        List<PhotoContestRelation> allContestantsSorted = photoContestRelationRepository.getSortedByResult(contestId);
        populateWinners(allContestantsSorted, firstPlaceWinners, secondPlaceWinners, thirdPlaceWinners);
        if (!firstPlaceWinners.isEmpty()) {
            providePointsToWinners(firstPlaceWinners, secondPlaceWinners, thirdPlaceWinners);
        }
    }

    private void populateWinners(
            List<PhotoContestRelation> allContestantsSorted,
            List<PhotoContestRelation> firstPlaceWinners,
            List<PhotoContestRelation> secondPlaceWinners,
            List<PhotoContestRelation> thirdPlaceWinners
    ) {
        int first = allContestantsSorted.get(0).getResult();
        int second = -1;
        int third = -1;

        for (PhotoContestRelation contestant : allContestantsSorted) {
            int num = contestant.getResult();

            if (num != first && second == -1) {
                second = num;
            }

            if (num != first && num != second && third == -1) {
                third = num;
            }

            if (num == first) {
                firstPlaceWinners.add(contestant);
            } else if (num == second) {
                secondPlaceWinners.add(contestant);
            } else if (num == third) {
                thirdPlaceWinners.add(contestant);
            }

            if (num < third) {
                break;
            }
        }
    }

    private void providePointsToWinners(
            List<PhotoContestRelation> firstPlaceWinners,
            List<PhotoContestRelation> secondPlaceWinners,
            List<PhotoContestRelation> thirdPlaceWinners
    ) {
        int firstPlacePoints;
        int secondPlacePoints;
        int thirdPlacePoints;

        if (firstPlaceWinners.size() > 1) {
            firstPlacePoints = 40;
        } else {
            firstPlacePoints = 50;
            if (!secondPlaceWinners.isEmpty()) {
                if (firstPlaceWinners.get(0).getResult() >= secondPlaceWinners.get(0).getResult() * 2) {
                    firstPlacePoints += 25;
                }
            }
        }
        if (secondPlaceWinners.size() > 1) {
            secondPlacePoints = 25;
        } else {
            secondPlacePoints = 35;
        }

        if (thirdPlaceWinners.size() > 1) {
            thirdPlacePoints = 10;
        } else {
            thirdPlacePoints = 20;
        }

        persistRespectivePlacePoints(firstPlaceWinners, firstPlacePoints);
        persistRespectivePlacePoints(secondPlaceWinners, secondPlacePoints);
        persistRespectivePlacePoints(thirdPlaceWinners, thirdPlacePoints);
    }

    private void persistRespectivePlacePoints(List<PhotoContestRelation> winningSubmissions, int pointsToGet) {
        for (PhotoContestRelation submission : winningSubmissions
        ) {
            User contestant = photoRepository.getById(submission.getPhotoId()).getAuthor();
            contestant.setPoints(contestant.getPoints() + pointsToGet);
            userRepository.update(contestant);
        }
    }

    private void addParticipationPoints(int photoId, Contest contest) {
        User newContestant = photoRepository.getById(photoId).getAuthor();
        if (contest.isOpen()) {
            newContestant.setPoints(newContestant.getPoints() + 1);
        } else {
            newContestant.setPoints(newContestant.getPoints() + 3);
        }
        userRepository.update(newContestant);
    }

    private void subtractParticipationPoints(int photoId, Contest contest) {
        User newContestant = photoRepository.getById(photoId).getAuthor();
        if (contest.isOpen()) {
            newContestant.setPoints(newContestant.getPoints() - 1);
        } else {
            newContestant.setPoints(newContestant.getPoints() - 3);
        }
        userRepository.update(newContestant);
    }
}


