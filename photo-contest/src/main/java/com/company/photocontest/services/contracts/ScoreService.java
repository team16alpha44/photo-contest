package com.company.photocontest.services.contracts;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.Score;
import com.company.photocontest.models.User;

import java.util.List;

public interface ScoreService {

    List<Score> getAll();

    Score getById(int id);

    List<Score> getByJuror(User juror);

    void delete(int id);

    void create(Score score, Photo photo, Contest contest, User executingUser);

    void update(Score score);
}
