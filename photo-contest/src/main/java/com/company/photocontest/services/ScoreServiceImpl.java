package com.company.photocontest.services;

import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.Score;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.ScoreRepository;
import com.company.photocontest.services.contracts.ScoreService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service


public class ScoreServiceImpl implements ScoreService {
    private final ScoreRepository scoreRepository;

    public ScoreServiceImpl(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    @Override
    public List<Score> getAll() {
        return scoreRepository.getAll();
    }

    @Override
    public Score getById(int id) {
        return scoreRepository.getById(id);
    }
    @Override
    public List<Score> getByJuror(User juror) {
        return scoreRepository.getByJuror(juror);
    }


    @Override
    public void delete(int id) {
        scoreRepository.delete(id);
    }

    @Override
    public void create(Score score, Photo photo, Contest contest, User executingUser) {
        if(!scoreRepository.getByJuror(executingUser).isEmpty()){
            throw new DuplicateEntityException("Score", "juror", executingUser.getUsername());
        }
        score.setJuror(executingUser);
        score.setPhoto(photo);
        score.setContest(contest);
        scoreRepository.create(score);
    }

    @Override
    public void update(Score score) {
        scoreRepository.update(score);
    }
}
