package com.company.photocontest.services.contracts;

import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProfilePictureService {

    ProfilePicture getById(int id);

    List<ProfilePicture> getAll();
    List<ProfilePicture> getByUser(User user);
    ProfilePicture getCurrent(User user);

    void create(ProfilePicture profilePicture, MultipartFile image, User executingUser);

    void update(ProfilePicture profilePicture);

    void delete(int id);

}
