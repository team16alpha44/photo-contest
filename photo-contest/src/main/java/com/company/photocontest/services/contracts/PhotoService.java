package com.company.photocontest.services.contracts;

import com.company.photocontest.models.Photo;
import com.company.photocontest.models.PhotoFilterOptions;
import com.company.photocontest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PhotoService {

    List<Photo> getAll();
    Photo getById(int id);
    Photo getByTitle(String title);
    List<Photo> getByAuthor(User author);

    Page<Photo> findPage(int pageNumber, PhotoFilterOptions photoFilterOptions);

    void delete(int id);

    void create(Photo photo, MultipartFile image, User author);

    void update(Photo photo, String oldImageName);
    void createCover(Photo photo, MultipartFile image, User author);


}
