package com.company.photocontest.services.contracts;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.ContestFilterOptions;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface ContestService {
    List<Contest> get(ContestFilterOptions contestFilterOptions);

    List<Contest> getAll();
    Contest getById(int id);

    Contest getByTitle(String title);

    List<Contest> getByCategory(String category);

    Photo getNumberOne(int contestId);

    void create(Contest contest, User user);


    void update(Contest contest);

    void delete(int id);

    void updateContestPhoto(int id, MultipartFile image, User user);

    void addPhotoToContest(int contestId, int photoId);

    void removePhotoFromContest(int contestId, int photoId);

    List<Contest> getByPhase(Phase phase);
    List<Contest> getByPhaseOneOpen();
    Page<Contest> findPage(int pageNumber, ContestFilterOptions contestFilterOptions);
    Page<Contest> findPageForParticipant(int pageNumber, ContestFilterOptions contestFilterOptions, int participantId);

    List<Contest> getByParticipant(int userId);
}
