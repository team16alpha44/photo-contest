package com.company.photocontest.services;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Category;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.CategoryRepository;
import com.company.photocontest.repositories.contracts.ContestRepository;
import com.company.photocontest.services.contracts.CategoryService;
import com.company.photocontest.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final String MODIFY_CONTEST_ERROR_MESSAGE = "Only organiser can modify the contest.";
    private final CategoryRepository categoryRepository;
    private final ContestRepository contestRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ContestRepository contestRepository) {
        this.categoryRepository = categoryRepository;
        this.contestRepository = contestRepository;
    }


    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }


    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }


    @Override
    public Category getByName(String name) {

        return categoryRepository.getByName(name);
    }


    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }


    @Override
    public void addCategoryToContest(String category) {
        try {
          categoryRepository.getByName(category);
        } catch (EntityNotFoundException e) {
            categoryRepository.create(new Category(category));
        }
    }

    private void checkModifyPermissions(User user) {
        if (!(user.isOrganiser())) {
            throw new AuthorizationException(MODIFY_CONTEST_ERROR_MESSAGE);
        }
    }
}
