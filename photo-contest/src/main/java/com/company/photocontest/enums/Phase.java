package com.company.photocontest.enums;

public enum Phase {
    PHASEONE,
    PHASETWO,
    FINAL;

    public String toStringFormatted() {
        return switch (this.ordinal()) {
            case 0 -> "Phase One";
            case 1 -> "Phase Two";
            case 2 -> "Final";
            default -> null;
        };
    }
}
