package com.company.photocontest.exceptions;

public class EntityNotFoundException extends RuntimeException{

    public EntityNotFoundException(String errorMessage) {
        super(String.format(errorMessage));
    }

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found.", type, attribute, value));
    }

    public EntityNotFoundException(String type, String attribute, String value, String in) {
        super(String.format("%s with %s %s not found in this %s.", type, attribute, value, in));
    }

}
