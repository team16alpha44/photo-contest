package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import com.company.photocontest.models.dto.UserFilterDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/contests/{contestId}/jury")
public class JuryMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestService contestService;
    private final ProfilePictureService profilePictureService;

    @Autowired
    public JuryMvcController(AuthenticationHelper authenticationHelper, UserService userService, ContestService contestService, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.contestService = contestService;
        this.profilePictureService = profilePictureService;
    }
    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/page/{currentPage}")
    public String getOnePage(HttpSession session,
                             Model model,
                             @PathVariable int contestId,
                             @PathVariable int currentPage,
                             @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            UserFilterOptions userFilterOptions = new UserFilterOptions(
                    userFilterDto.getUsername(),
                    userFilterDto.getFirstName(),
                    userFilterDto.getLastName(),
                    userFilterDto.getMinPoints(),
                    userFilterDto.getMaxPoints(),
                    userFilterDto.getSortBy(),
                    userFilterDto.getSortOrder(),
                    userFilterDto.getOrganiser());

            Contest contest = contestService.getById(contestId);
            Page<User> page = userService.findPageInContest(currentPage, userFilterOptions, contest);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<User> users = page.getContent();

            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("userFilterOptions", userFilterDto);
            model.addAttribute("users", users);
            model.addAttribute("contest", contest);
            model.addAttribute("username", userFilterDto.getUsername());
            model.addAttribute("firstName", userFilterDto.getFirstName());
            model.addAttribute("lastName", userFilterDto.getLastName());
            return "JuryContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/page/{currentPage}/{username}&{firstName}&{lastName}")
    public String getOnePageFiltered(HttpSession session,
                             Model model,
                             @PathVariable int contestId,
                             @PathVariable int currentPage,
                             @PathVariable String username,
                             @PathVariable String firstName,
                             @PathVariable String lastName,
                             @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            UserFilterOptions userFilterOptions = new UserFilterOptions(
                    username,
                    firstName,
                    lastName,
                    userFilterDto.getMinPoints(),
                    userFilterDto.getMaxPoints(),
                    userFilterDto.getSortBy(),
                    userFilterDto.getSortOrder(),
                    userFilterDto.getOrganiser());

            Contest contest = contestService.getById(contestId);
            Page<User> page = userService.findPageInContest(currentPage, userFilterOptions, contest);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<User> users = page.getContent();

            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("userFilterOptions", userFilterDto);
            model.addAttribute("users", users);
            model.addAttribute("contest", contest);
            return "JuryContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping
    public String getAllPages(HttpSession session,
                              Model model,
                              @PathVariable int contestId,
                              @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        return getOnePage(session, model, contestId, 1, userFilterDto);
    }
}
