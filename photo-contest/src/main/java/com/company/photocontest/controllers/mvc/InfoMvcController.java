package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.services.contracts.ProfilePictureService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class InfoMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final ProfilePictureService profilePictureService;
    @Autowired
    public InfoMvcController(AuthenticationHelper authenticationHelper, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/faq")
    public String showContactsPage(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            return "FAQView";
        }
        return "FAQView";
    }
}
