package com.company.photocontest.controllers.rest;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ProfilePictureMapper;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.ProfilePictureDto;
import com.company.photocontest.payload.FileResponse;
import com.company.photocontest.services.contracts.ProfilePictureService;
import jakarta.validation.Valid;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/profile-pictures")
public class ProfilePictureRestController {

    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    public static final String SUCCESSFULLY_UPLOADED = "Profile picture successfully uploaded.";
    private final AuthenticationHelper authenticationHelper;
    private final ProfilePictureService profilePictureService;
    private final ProfilePictureMapper profilePictureMapper;

    public ProfilePictureRestController(AuthenticationHelper authenticationHelper, ProfilePictureService profilePictureService, ProfilePictureMapper profilePictureMapper) {
        this.authenticationHelper = authenticationHelper;
        this.profilePictureService = profilePictureService;
        this.profilePictureMapper = profilePictureMapper;
    }

    @GetMapping()
    public List<ResponseEntity<FileResponse>> getByUser(@RequestHeader HttpHeaders headers) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            List<ProfilePicture> allPictures = profilePictureService.getByUser(executingUser);
            List<ResponseEntity<FileResponse>> responseEntities = new ArrayList<>();
            allPictures.forEach(x -> responseEntities.add(new ResponseEntity<>(
                    new FileResponse(
                            x.getId(),
                            x.getTitle(),
                            SUCCESSFULLY_UPLOADED),
                    HttpStatus.OK)));
            return responseEntities;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<FileResponse> getById(@RequestHeader HttpHeaders headers,
                                                @PathVariable int id){
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            ProfilePicture repositoryPicture = profilePictureService.getById(id);
            checkPermissions(repositoryPicture.getUser().getId(), executingUser);
            return new ResponseEntity<>(new FileResponse(repositoryPicture.getId(), repositoryPicture.getTitle(), SUCCESSFULLY_UPLOADED),
                    HttpStatus.OK);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<FileResponse> create(@RequestHeader HttpHeaders headers,
                                               @Valid @RequestPart String title,
                                               @RequestPart MultipartFile image) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            ProfilePicture profilePicture = profilePictureMapper.dtoToProfilePicture(new ProfilePictureDto(title));
            profilePictureService.create(profilePicture, image, executingUser);
            return new ResponseEntity<>(new FileResponse(profilePicture.getId(), profilePicture.getTitle(), SUCCESSFULLY_UPLOADED),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{userId}/select/{picId}")
    public void selectProfilePicture(@RequestHeader HttpHeaders headers,@PathVariable int userId, @PathVariable int picId){
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            ProfilePicture newProfilePicture = profilePictureService.getById(picId);
            checkPermissions(newProfilePicture.getUser().getId(), executingUser);
            newProfilePicture.setActive(true);
            profilePictureService.update(newProfilePicture);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkPermissions(profilePictureService.getById(id).getUser().getId(), executingUser);
            profilePictureService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    private static void checkPermissions(int targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {

            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
}
