package com.company.photocontest.controllers.mvc;


import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.UserMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.UserFilterDto;
import com.company.photocontest.models.dto.EditProfileDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.services.contracts.UserService;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    public static final String ERROR_MESSAGE = "You cannot edit other users' data.";
    private static final String INVALID_AUTHORIZATION_ERROR = "Invalid authorization.";

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final PhotoService photoService;
    private final ContestService contestService;
    private final ProfilePictureService profilePictureService;



    public UserMvcController(AuthenticationHelper authenticationHelper,
                             UserService userService,
                             UserMapper userMapper,
                             PhotoService photoService,
                             ContestService contestService,
                             ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.photoService = photoService;
        this.contestService = contestService;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganiser")
    public boolean isOrganiser(HttpSession session) {
        if(session.getAttribute("isOrganiser") != null)
            return (boolean) session.getAttribute("isOrganiser");
        return false;
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable int id, Model model, HttpSession session) {
        User currentUser=null;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        User user = userService.getById(id);
        List<Photo> photos = photoService.getByAuthor(user);
        List<Contest> contests = contestService.getByParticipant(id);
        List<Photo> fourPhotos = photos.stream().limit(4).toList();
        ProfilePicture profilePicture = profilePictureService.getCurrent(user);
        model.addAttribute("profilePicture", profilePicture);
        model.addAttribute("user", user);
        model.addAttribute("photos", photos);
        model.addAttribute("contests", contests);
        model.addAttribute("fourPhotos", fourPhotos);
        return "MyProfileView";
    }

    @GetMapping("/{id}/update")
    public String showUserUpdate(@PathVariable int id, HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            return "error-404";
        }
        try {
            checkPermissions(id, currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        User user = userService.getById(id);
        model.addAttribute("user", userMapper.userToEditProfileDto(user));
        return "EditProfileView";
    }



    @PostMapping("{id}/update")
    public String handleUserUpdate(@PathVariable int id,
                             @Valid @ModelAttribute("user") EditProfileDto userDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", user);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "EditProfileView";
        }
        if (!BCrypt.checkpw(userDto.getOldPassword(), user.getPassword())) {
            bindingResult.rejectValue("oldPassword", "password_error", "Wrong password. Try again.");
            return "EditProfileView";
        }
        if (!userDto.getNewPassword().equals(userDto.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error", "New password and confirm password should match.");
            return "EditProfileView";
        }
        try {
            User userForUpdate = userMapper.editProfileDtoToUser(userDto, id);
            userService.update(userForUpdate);
            return String.format("redirect:/users/%d", id);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "EditProfileView";
        }
    }


    @GetMapping
    public String showAllUsers(@ModelAttribute("userFilterOptions") UserFilterDto userFilterDto, Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
        model.addAttribute("profilePicture", profilePicture);
        try {
            if(!currentUser.isOrganiser())
                throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        UserFilterOptions userFilterOptions = new UserFilterOptions(
                userFilterDto.getUsername(),
                userFilterDto.getFirstName(),
                userFilterDto.getLastName(),
                userFilterDto.getMinPoints(),
                userFilterDto.getMaxPoints(),
                userFilterDto.getSortBy(),
                userFilterDto.getSortOrder(),
                userFilterDto.getOrganiser());
        model.addAttribute("currentUser", currentUser);
        List<User> users = userService.get(userFilterOptions);
        model.addAttribute("userFilterOptions", userFilterDto);
        model.addAttribute("users", users);
        model.addAttribute("profilePictureService", profilePictureService);
        return "AllPhotoJunkiesView";
    }

    @GetMapping("/{id}/make-organiser")
    public String makeOrganiserUser(@PathVariable int id,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            User userToOrganiser = userService.getById(id);
            userToOrganiser.setOrganiser(true);
            userService.update(userToOrganiser);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }


    private static void checkPermissions(int targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
}
