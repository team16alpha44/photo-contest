package com.company.photocontest.controllers.rest;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ContestMapper;
import com.company.photocontest.helpers.PhotoMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.ContestDto;
import com.company.photocontest.models.dto.PhotoDto;
import com.company.photocontest.payload.FileResponse;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/contests")
public class    ContestRestController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    public static final String USER_ADDED_TO_JURY_OF_CONTEST = "User %d added to jury of contest %d";
    public static final String USER_REMOVED_FROM_JURY_OF_CONTEST = "User %d removed from jury of contest %d";
    public static final String NOT_ELIGIBLE_FOR_JURY = "User does not have enough points accumulated to be a jury.";
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final ContestMapper contestMapper;
    private final PhotoService photoService;
    private final UserService userService;

    public ContestRestController(AuthenticationHelper authenticationHelper,
                                 ContestService contestService, ContestMapper contestMapper,
                                 PhotoService photoService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.contestMapper = contestMapper;
        this.photoService = photoService;
        this.userService = userService;
    }

    @GetMapping
    public List<Contest> getAll() {
        return contestService.getAll();
    }


    @GetMapping("/{id}")
    public Contest getById(@PathVariable Integer id) {
        try {
            return contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{title}")
    public Contest getTitle(@PathVariable String title) {
        return contestService.getByTitle(title);
    }

    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers, @RequestBody @Valid ContestDto contestDto) {
        try {

            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.dtoToContest(contestDto);
            contestService.create(contest, user);

            return contest;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping({"/{contestId}/photos/{photoId}"})
    public ResponseEntity<FileResponse> getPhotoById(@RequestHeader HttpHeaders headers,
                                                     @PathVariable int contestId,
                                                     @PathVariable int photoId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoInContest(contest, photoId);
            return new ResponseEntity<>(new FileResponse(photo.getId(), photo.getTitle(),
                    photo.getStory(), "Image is successfuly sent."),
                    HttpStatus.OK);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping({"/{contestId}/photos"})
    public List<ResponseEntity<FileResponse>> getAllPhotos(@RequestHeader HttpHeaders headers,
                                                           @PathVariable int contestId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            Set<Photo> allPhotos = contest.getPhotos().stream().map(p -> photoService.getById(p.getPhotoId())).collect(Collectors.toSet());
            List<ResponseEntity<FileResponse>> responseEntities = new ArrayList<>();
            allPhotos.forEach(x -> responseEntities.add(new ResponseEntity<>(
                    new FileResponse(
                            x.getId(),
                            x.getTitle(),
                            x.getStory(),
                            "Image is successfuly sent."),
                    HttpStatus.OK)));
            return responseEntities;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{contestId}/jury/{userId}/add")
    public ResponseEntity addJuryToContest(@RequestHeader HttpHeaders headers,
                                           @PathVariable int contestId,
                                           @PathVariable int userId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            User jurorToAdd = userService.getById(userId);
            checkIEligibleForJury(jurorToAdd);
            Contest contest = contestService.getById(contestId);
            contest.addJuryToContest(jurorToAdd);
            contestService.update(contest);
            return new ResponseEntity<>(
                    String.format(USER_ADDED_TO_JURY_OF_CONTEST, userId, contestId),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{contestId}/jury/{userId}")
    public ResponseEntity removeJuryFromContest(@RequestHeader HttpHeaders headers,
                                                @PathVariable int contestId,
                                                @PathVariable int userId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            User jurorToRemove = userService.getById(userId);
            Contest contest = contestService.getById(contestId);
            contest.removeJuror(jurorToRemove);
            contestService.update(contest);
            return new ResponseEntity<>(
                    String.format(USER_REMOVED_FROM_JURY_OF_CONTEST, userId, contestId),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private static void checkIEligibleForJury(User targetUser) {
        if (targetUser.getPoints() < 151) {
            throw new AuthorizationException(NOT_ELIGIBLE_FOR_JURY);
        }
    }

    private static void checkIfOrganiserOrJuryOrFinalPhase(User executingUser, Contest contest) {
        if (!(executingUser.isOrganiser() || contest.getJurors().contains(executingUser))) {
            if (checkContestPhase(contest))
                return;
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static void checkOrganiserPermissions(User executingUser) {
        if (!executingUser.isOrganiser()) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static void checkJuryPermissions(Contest contest, User executingUser) {
        if (!contest.getJurors().contains(executingUser)) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static boolean checkContestPhase(Contest contest) {
        if (!contest.getPhase().equals(Phase.FINAL)) {
            if (contest.getExpiredDatePhase2().isAfter(LocalDateTime.now())) {
                contest.setPhase(Phase.FINAL);
                return true;
            }
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contest is not finished yet.");
    }

    private static void checkIfPhotoInContest(Contest contest, int photoId) {
        if (contest.getPhotos().stream().noneMatch(p -> p.getPhotoId() == photoId))
            throw new EntityNotFoundException("No such image in this contest.");
    }

    @PatchMapping ("/{id}/update")
    public ResponseEntity updateContest(@RequestHeader HttpHeaders headers,
                                        @PathVariable int id,
                                        @RequestPart MultipartFile image) {

        User executingUser = authenticationHelper.tryGetUser(headers);
        contestService.updateContestPhoto(id, image, executingUser);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
