package com.company.photocontest.controllers.rest;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ScoreMapper;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.Score;
import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.ScoreDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.ScoreService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/api/contests/{contestId}/photos/{photoId}")
public class ScoreRestController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    public static final String SCORE_NOT_FOUND_IN_THIS_CONTEST_PHOTO = "Score with id %s not found in this contest photo";
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final ScoreService scoreService;
    private final ScoreMapper scoreMapper;
    private final UserService userService;
    @Autowired
    public ScoreRestController(ScoreService scoreService, ScoreMapper scoreMapper,
                               AuthenticationHelper authenticationHelper,
                               ContestService contestService, PhotoService photoService,
                               UserService userService) {
        this.scoreService = scoreService;
        this.scoreMapper = scoreMapper;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.photoService = photoService;
        this.userService = userService;
    }

    @GetMapping("/score/{scoreId}")
    public Score getById(@RequestHeader HttpHeaders headers,
                         @PathVariable int contestId,
                         @PathVariable int photoId,
                         @PathVariable int scoreId){
        try{
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoInContest(contest, photoId);
            checkIfScoreBelongsToPhoto(contest, photo, scoreId);
            Score score = scoreService.getById(scoreId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            return score;
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/score/juror/{jurorId}")
    public Score getByJuror(@RequestHeader HttpHeaders headers,
                            @PathVariable int contestId,
                            @PathVariable int photoId,
                            @PathVariable int jurorId){
        try{
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            Photo photo = photoService.getById(photoId);
            User juror = userService.getById(jurorId);
            Score score = scoreService.getByJuror(juror).stream()
                    .filter(x->x.getPhoto().getId() == photoId)
                    .findFirst().get();
            checkIfScoreBelongsToPhoto(contest, photo, score.getId());
            return score;
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/score")
    public List<Score> getAll(@RequestHeader HttpHeaders headers,
                              @PathVariable int contestId,
                              @PathVariable int photoId){
        try{
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            return scoreService.getAll().stream()
                    .filter(x->x.getPhoto().getId() == photoId)
                    .filter(x->x.getPhoto().getContests().contains(contest))
                    .collect(Collectors.toList());
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/score")
    public Score create(@RequestHeader HttpHeaders headers,
                        @Valid @RequestBody ScoreDto scoreDto,
                        @PathVariable int contestId,
                        @PathVariable int photoId) {
        try {

            User executingUser = authenticationHelper.tryGetUser(headers);
            Score score = scoreMapper.dtoToScore(scoreDto);
            Contest contest = contestService.getById(contestId);
            checkContestPhaseIfSecond(contest);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoInContest(contest, photoId);
            scoreService.create(score, photo, contest, executingUser);
            return score;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (EntityNotFoundException e ){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND , e.getMessage());
        }
    }

    @PutMapping("/score/{scoreId}")
    public Score update(@RequestHeader HttpHeaders headers,
                        @PathVariable int contestId,
                        @PathVariable int photoId,
                        @PathVariable int scoreId,
                        @Valid @RequestBody ScoreDto scoreDto) {
        try {
            authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkContestPhaseIfSecond(contest);
            Photo photo = photoService.getById(photoId);
            Score scoreToUpdate = scoreService.getById(scoreId);
            checkIfPhotoInContest(contest, photoId);
            checkIfScoreBelongsToPhoto(contest, photo, scoreToUpdate.getId());
            scoreMapper.dtoToUpdateScore(scoreToUpdate, scoreDto);
            scoreService.update(scoreToUpdate);
            return scoreToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/score/{scoreId}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int contestId,
                       @PathVariable int photoId,
                       @PathVariable int scoreId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            checkContestPhaseIfSecond(contest);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoInContest(contest, photoId);
            checkIfScoreBelongsToPhoto(contest, photo, scoreId);
            checkIfOrganiserOrJuryOrFinalPhase(executingUser, contest);
            scoreService.delete(scoreId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    private static boolean checkIfScoreBelongsToPhoto(Contest contest, Photo photo, int photoScoreId){
        if(contest.getPhotos().contains(photo)){
            if(photo.getId() == photoScoreId){
                return true;
            }
        }
        throw new EntityNotFoundException(SCORE_NOT_FOUND_IN_THIS_CONTEST_PHOTO, photoScoreId);
    }

    private static void checkIfOrganiserOrJuryOrFinalPhase(User executingUser, Contest contest) {
        if(!(executingUser.isOrganiser() || contest.getJurors().contains(executingUser))){
            if(checkContestPhaseForJunkie(contest))
                return;
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
    private static void checkContestPhaseIfSecond(Contest contest){
        if (!contest.getPhase().equals(Phase.PHASETWO)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contest is not in Phase 2 yet.");
        }
    }
    private static boolean checkContestPhaseForJunkie(Contest contest) {
        if (!contest.getPhase().equals(Phase.FINAL)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contest is not finished yet.");
        }
        return true;
    }

    private static void checkIfPhotoInContest(Contest contest, int photoId) {
        if (contest.getPhotos().stream().noneMatch(p->p.getPhotoId()==photoId))
            throw new EntityNotFoundException("No such image in this contest.");
    }
}
