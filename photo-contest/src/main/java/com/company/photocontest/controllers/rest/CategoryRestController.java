package com.company.photocontest.controllers.rest;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.CategoryMapper;
import com.company.photocontest.models.Category;
import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.CategoryDto;
import com.company.photocontest.services.contracts.CategoryService;
import com.company.photocontest.services.contracts.ContestService;
import jakarta.validation.Valid;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/contests/{contestId}/categories")
public class CategoryRestController {
    public static final String UNAUTHORIZED_OPERATION = "Only post organiser  can perform this action.";
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;

    public CategoryRestController(CategoryService categoryService, CategoryMapper categoryMapper,
                                  ContestService contestService, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Category getCategory(@RequestHeader HttpHeaders headers, @PathVariable int contestId){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return   contestService.getById(contestId).getCategory();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


//    @PostMapping
//    public void addCategoryToContest(@PathVariable int contestId, @RequestHeader HttpHeaders headers
//            , @Valid @RequestBody CategoryDto categoryDto) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            Category category = categoryMapper.dtoToCategory(categoryDto);
////            categoryService.addCategoryToContest(category , contestId , user);
//
//        } catch (AuthorizationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//
//    }

}
