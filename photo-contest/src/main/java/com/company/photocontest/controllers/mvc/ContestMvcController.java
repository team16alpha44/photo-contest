package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ContestMapper;
import com.company.photocontest.helpers.PhotoMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.ContestDto;
import com.company.photocontest.models.dto.ContestFilterDto;
import com.company.photocontest.services.contracts.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {
    private final ContestService contestService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestMapper contestMapper;
    private final CategoryService categoryService;
    private final ProfilePictureService profilePictureService;
    private final PhotoService photoService;


    private final static String ERROR_MESSAGE = "You are not authorized to execute this organiser operation.";

    @Autowired
    public ContestMvcController(ContestService contestService, UserService userService,
                                AuthenticationHelper authenticationHelper, ContestMapper contestMapper,
                                CategoryService categoryService,  ProfilePictureService profilePictureService, PhotoService photoService) {
        this.contestService = contestService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
        this.categoryService = categoryService;
        this.photoService = photoService;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser")
                != null;
    }

    @ModelAttribute("isOrganiser")
    public boolean isOrganiserr(HttpSession session) {
        if (session.getAttribute("isOrganiser") != null)
            return (boolean) session.getAttribute("isOrganiser");
        return false;
    }

    @ModelAttribute("categories")
    public List<Category> categories() {
        return categoryService.getAll();
    }


    @ModelAttribute("recentContests")
    public List<Contest> recentContest() {

        return contestService.getAll().stream()
                .filter(contest -> {
                    LocalDate postDate = contest.getCreatedDate().toLocalDate();
                    long daysBetween = ChronoUnit.DAYS.between(postDate, LocalDateTime.now());
                    return daysBetween >= 0 && daysBetween <= 10;
                })
                .sorted(Comparator.comparing(Contest::getCreatedDate).reversed())
                .limit(3)
                .toList();
    }

    @GetMapping
    public String showAllContests(@ModelAttribute("filterOptions") ContestFilterDto contestFilterDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", user);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                contestFilterDto.getTitle(),
                contestFilterDto.getCategory(),
                contestFilterDto.getPhase()
        );

        List<Contest> contests = contestService.get(contestFilterOptions);
        ProfilePicture profilePicture = profilePictureService.getCurrent(user);
        model.addAttribute("profilePicture", profilePicture);
        model.addAttribute("filterOptions", contestFilterDto);
        model.addAttribute("contests", contests);
        return "ContestsView";
    }

    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", user);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {

            model.addAttribute("filterOptions", new ContestFilterDto());
            List<Contest> contests = contestService.get(new ContestFilterOptions());
            Contest contest = contestService.getById(id);
            ProfilePicture profilePicture = profilePictureService.getCurrent(user);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("contest", contest);
            model.addAttribute("category", contest.getCategory());
            model.addAttribute("contests", contests);
            Set<Photo> contestPhotos = new HashSet<>(contest.getPhotos().stream().map(x -> photoService.getById(x.getPhotoId())).toList());
            model.addAttribute("contestPhotos" , contestPhotos);
            return "ContestSingleView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/new")
    public String showNewContestPage(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);

        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            ContestDto contestDto = new ContestDto();
            model.addAttribute("contestDto", contestDto);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            return "CreateContestsView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }


    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contest") ContestDto contestDto,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", user);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }


        try {
            Contest contest = contestMapper.dtoToContest(contestDto);
            contestService.create(contest, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }


    @GetMapping("/{id}/update")
    public String showUpdateContestPage(@PathVariable int id,

                                        HttpSession session, Model model

    ) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);

        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            Contest contest = contestService.getById(id);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("contestid", id);
            model.addAttribute("contest", contest);
            return "UpdateContestsView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @PostMapping ("/{id}/update")
    public String updateContest(@PathVariable String id, HttpSession session, Model model,
                                @ModelAttribute MultipartFile image){
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", user);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        try {
            if (image.isEmpty()) {
                model.addAttribute("error", "Please select a file to upload.");
                return "error-404";
            }
            model.addAttribute("contest" , contestService.getById(Integer.parseInt(id)));
            contestService.updateContestPhoto(Integer.parseInt(id), image, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
}
