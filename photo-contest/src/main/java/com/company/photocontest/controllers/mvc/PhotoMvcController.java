package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.PhotoMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.ContestFilterDto;
import com.company.photocontest.models.dto.PhotoDto;
import com.company.photocontest.models.dto.PhotoFilterDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/users/{userId}/photos")
public class PhotoMvcController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final PhotoService photoService;
    private final ContestService contestService;
    private final PhotoMapper photoMapper;
    private final ProfilePictureService profilePictureService;
    @Autowired
    public PhotoMvcController(AuthenticationHelper authenticationHelper, UserService userService, PhotoService photoService, ContestService contestService, PhotoMapper photoMapper, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.photoService = photoService;
        this.contestService = contestService;
        this.photoMapper = photoMapper;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganiser")
    public boolean populateIsOrganiser(HttpSession session) {
        if (session.getAttribute("isOrganiser") != null)
            return (boolean) session.getAttribute("isOrganiser");
        return false;
    }

    @GetMapping("/page/{currentPage}")
    public String getOnePage(HttpSession session,
                             Model model,
                             @PathVariable int userId,
                             @PathVariable int currentPage,
                             @ModelAttribute("photoFilterOptions") PhotoFilterDto photoFilterDto) {
        User currentUser;
        User authorUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            authorUser = userService.getById(userId);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("authorUser", authorUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            PhotoFilterOptions photoFilterOptions = new PhotoFilterOptions(
                    photoFilterDto.getTitle(),
                    photoFilterDto.getStory(),
                    authorUser.getId(),
                    photoFilterDto.getSortBy(),
                    photoFilterDto.getSortOrder());
            Page<Photo> page = photoService.findPage(currentPage, photoFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Photo> photos = page.getContent();

            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("photos", photos);
            model.addAttribute("title", photoFilterDto.getTitle());
            model.addAttribute("story", photoFilterDto.getStory());
            return "UserGalleryView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/page/{currentPage}/{title}&{story}")
    public String getOnePageFiltered(HttpSession session,
                                     Model model,
                                     @PathVariable int userId,
                                     @PathVariable int currentPage,
                                     @PathVariable String title,
                                     @PathVariable String story,
                                     @ModelAttribute("photoFilterOptions") PhotoFilterDto photoFilterDto) {
        User currentUser;
        User authorUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            authorUser = userService.getById(userId);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("authorUser", authorUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            PhotoFilterOptions photoFilterOptions = new PhotoFilterOptions(
                    title,
                    story,
                    authorUser.getId(),
                    photoFilterDto.getSortBy(),
                    photoFilterDto.getSortOrder());
            Page<Photo> page = photoService.findPage(currentPage, photoFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Photo> photos = page.getContent();

            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("photos", photos);
            return "UserGalleryView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping
    public String getAllPages(HttpSession session,
                              Model model,
                              @PathVariable int userId,
                              @ModelAttribute("photoFilterOptions") PhotoFilterDto photoFilterDto) {
        return getOnePage(session, model, userId, 1, photoFilterDto);
    }

    @GetMapping("/{photoId}")
    public String showPhotoPage(HttpSession session, Model model,
                                @PathVariable int userId,
                                @PathVariable int photoId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            User author = userService.getById(userId);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoBelongsToUser(author, photo);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("isAuthor", checkIfPhotoAuthor(currentUser, photo));
            model.addAttribute("authorUser", author);
            model.addAttribute("photo", photo);
            model.addAttribute("photoContests",
                    photo.getContests().stream().map(contestService::getById).collect(Collectors.toSet()));
            return "PhotoView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/{photoId}/edit")
    public String showPhotoEditPage(HttpSession session, Model model,
                                    @PathVariable int userId,
                                    @PathVariable int photoId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            User author = userService.getById(userId);
            checkIfAuthorOrOrganiser(currentUser, author);
            Photo photo = photoService.getById(photoId);
            checkIfPhotoBelongsToUser(author, photo);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("isAuthor", checkIfPhotoAuthor(currentUser, photo));
            model.addAttribute("authorUser", author);
            model.addAttribute("photo", photo);
            PhotoDto photoDto = new PhotoDto(photo.getTitle(), photo.getStory());
            model.addAttribute("photoDto", photoDto);
            return "PhotoEditView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @PostMapping("/{photoId}/edit")
    public String editPhoto(HttpSession session, Model model,
                            @Valid @ModelAttribute("photoDto") PhotoDto photoDto,
                            BindingResult bindingResult,
                            @PathVariable int userId,
                            @PathVariable int photoId) {
        User currentUser;
        User author;
        Photo photoToUpdate;
        try {
            photoToUpdate = photoService.getById(photoId);
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            author = userService.getById(userId);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        if (bindingResult.hasErrors()) {
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("isAuthor", checkIfPhotoAuthor(currentUser, photoToUpdate));
            model.addAttribute("authorUser", author);
            model.addAttribute("photo", photoToUpdate);
            return "PhotoEditView";
        }

        try {
            checkIfAuthorOrOrganiser(currentUser, author);
            String oldImageName = "\\" + photoToUpdate.getTitle() + ".jpg";
            photoMapper.dtoToUpdatePhoto(photoToUpdate, photoDto);
            photoService.update(photoToUpdate, oldImageName);
            return "redirect:/users/{userId}/photos/{photoId}";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @PostMapping("/new")
    public String newPhoto(HttpSession session, Model model,
                           @Valid @ModelAttribute("photoDto") PhotoDto photoDto,
                           @RequestPart("image") MultipartFile image,
                           BindingResult bindingResult,
                           @PathVariable int userId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        if (bindingResult.hasErrors()) {
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            return "PhotoNewView";
        }

        try {
            if (image.isEmpty()) {
                model.addAttribute("error", "Please select a file to upload.");
                return "error-404";
            }
            Photo photo = photoMapper.dtoToPhoto(photoDto);
            photoService.create(photo, image, currentUser);
            return "redirect:/users/{userId}/photos";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/new")
    public String showNewPhotoPage(HttpSession session, Model model,
                                   @PathVariable int userId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!(currentUser.getId() == userId)) {
                throw new AuthorizationException(ERROR_MESSAGE);
            }
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            PhotoDto photoDto = new PhotoDto();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("photoDto", photoDto);
            return "PhotoNewView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    //    @GetMapping("/new2")
//    public ResponseEntity sdfs(){
//        File myObj = new File("photo-contest\\test.jpg");
////        InputStream in = getClass()
////                .getResourceAsStream("photo-contest\\test.jpg");
//        InputStream targetStream = null;
//        try {
//            targetStream = new FileInputStream(myObj);
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        }
//        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
//                .body(new InputStreamResource(targetStream));
//    }
    @GetMapping("/{photoId}/delete")
    public String deletePhoto(@PathVariable int userId, @PathVariable int photoId,
                              Model model,
                              HttpSession session) {
        User currentUser;
        User authorUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            authorUser = userService.getById(userId);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("authorUser", authorUser);
            checkIfAuthorOrOrganiser(currentUser, authorUser);
        } catch (AuthorizationException e) {
            return "error-404";
        }

        try {
            photoService.delete(photoId);
            return "redirect:/users/{userId}/photos";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/{photoId}/openContests/page/{currentPage}")
    public String getOnePageOpenContests(HttpSession session,
                                     Model model,
                                     @PathVariable int photoId,
                                     @PathVariable int userId,
                                     @PathVariable int currentPage,
                                     @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        Photo photo;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            photo = photoService.getById(photoId);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategory(),
                    "PHASEONE");

            checkIfPhotoBelongsToUser(photo.getAuthor(), photo);
            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("photo", photo);
            model.addAttribute("title", contestFilterDto.getTitle());
            model.addAttribute("category", contestFilterDto.getCategory());
            return "AddPhotoToContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/{photoId}/openContests/page/{currentPage}/{title}&{category}")
    public String getOnePageOpenContestsFiltered(HttpSession session,
                                     Model model,
                                     @PathVariable int photoId,
                                     @PathVariable int userId,
                                     @PathVariable int currentPage,
                                     @PathVariable String title,
                                     @PathVariable String category,
                                     @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        Photo photo;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            photo = photoService.getById(photoId);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    title,
                    category,
                    "PHASEONE");

            checkIfPhotoBelongsToUser(photo.getAuthor(), photo);
            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("photo", photo);
            return "AddPhotoToContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/{photoId}/openContests")
    public String getAllPagesOpenContests(HttpSession session,
                                      Model model,
                                      @PathVariable int photoId,
                                      @PathVariable int userId,
                                      @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        return getOnePageOpenContests(session, model, photoId, userId, 1, contestFilterDto);
    }

    @GetMapping("/{photoId}/openContests/{contestId}")
    public String addPhotoToContest(@PathVariable int userId, @PathVariable int photoId, @PathVariable int contestId,
                                    Model model,
                                    HttpSession session) {
        User currentUser;
        User authorUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            authorUser = userService.getById(userId);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("authorUser", authorUser);
            checkIfAuthorOrOrganiser(currentUser, authorUser);
        } catch (AuthorizationException e) {
            return "error-404";
        }

        try {
            contestService.addPhotoToContest(contestId, photoId);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            return "redirect:/users/{userId}/photos";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    private static boolean checkIfPhotoAuthor(User executingUser, Photo photo) {
        return photo.getAuthor().getId() == executingUser.getId();
    }

    private static void checkIfPhotoBelongsToUser(User executingUser, Photo photo) {
        if (!checkIfPhotoAuthor(executingUser, photo)) {
            throw new EntityNotFoundException("Photo", photo.getId());
        }
    }

    private static void checkIfAuthorOrOrganiser(User currentUser, User authorUser) {
        if (!(currentUser.isOrganiser() || currentUser.getId() == authorUser.getId())) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
}
