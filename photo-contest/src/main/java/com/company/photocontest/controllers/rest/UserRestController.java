package com.company.photocontest.controllers.rest;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.UserMapper;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import com.company.photocontest.models.dto.RegisterDto;
import com.company.photocontest.models.dto.UserOutDto;
import com.company.photocontest.models.dto.UserUpdateDto;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<UserOutDto> get(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) String username,
                                @RequestParam(required = false) String firstName,
                                @RequestParam(required = false) String lastName,
                                @RequestParam(required = false) Integer minPoints,
                                @RequestParam(required = false) Integer maxPoints,
                                @RequestParam(required = false) String sortBy,
                                @RequestParam(required = false) String sortOrder,
                                @RequestParam(required = false) Boolean isOrganiser) {
        UserFilterOptions userFilterOptions = new UserFilterOptions(username, firstName, lastName, minPoints, maxPoints, sortBy, sortOrder, isOrganiser);

        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            List<User> users = userService.get(userFilterOptions);
            return users.stream().map(userMapper::userToOutDto).collect(Collectors.toList());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{id}")
    public UserOutDto getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            return userMapper.userToOutDto(userService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public UserOutDto create(@Valid @RequestBody RegisterDto registerDto) {
        try {
            User user = userMapper.dtoToUser(registerDto);
            userService.create(user);
            return userMapper.userToOutDto(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserOutDto update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserUpdateDto userUpdateDto) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkJunkiePermissions(id, executingUser);
            User userToUpdate = userMapper.dtoToUser(id, userUpdateDto);
            userService.update(userToUpdate);
            return userMapper.userToOutDto(userToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkJunkiePermissions(id, executingUser);
            userService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/make-organiser")
    public void makeOrganiser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            User junkieToOrganiser = userService.getById(id);
            junkieToOrganiser.setOrganiser(true);
            userService.update(junkieToOrganiser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    private static void checkJunkiePermissions(int targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static void checkOrganiserPermissions(User executingUser) {
        if (!executingUser.isOrganiser()) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
}
