package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.ContestFilterDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/my")
public class DashboardMvcController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final ProfilePictureService profilePictureService;

    @Autowired
    public DashboardMvcController(AuthenticationHelper authenticationHelper,
                                  ContestService contestService, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/organiser/page/{currentPage}")
    public String getOnePageOrganiserContests(HttpSession session,
                                         Model model,
                                         @PathVariable int currentPage,
                                         @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(!currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategory(),
                    contestFilterDto.getPhase());

            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("title", contestFilterDto.getTitle());
            model.addAttribute("category", contestFilterDto.getCategory());
            model.addAttribute("phase", contestFilterDto.getPhase());
            return "DashboardViewOrganiser";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/organiser/page/{currentPage}/{title}&{category}&{phase}")
    public String getOnePageOrganiserContestsFiltered(HttpSession session,
                                                 Model model,
                                                 @PathVariable int currentPage,
                                                 @PathVariable String title,
                                                 @PathVariable String category,
                                                 @PathVariable String phase,
                                                 @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(!currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    title,
                    category,
                    phase);

            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            return "DashboardViewOrganiser";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/organiser")
    public String getAllPagesOrganiserContests(HttpSession session,
                                               Model model,
                                               @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        return getOnePageOrganiserContests(session, model, 1, contestFilterDto);
    }

    @GetMapping("/junkie/page/{currentPage}")
    public String getOnePageJunkieContests(HttpSession session,
                                              Model model,
                                              @PathVariable int currentPage,
                                              @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategory(),
                    "PHASEONE");

            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("title", contestFilterDto.getTitle());
            model.addAttribute("category", contestFilterDto.getCategory());
            return "DashboardViewJunkie";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie/page/{currentPage}/{title}&{category}")
    public String getOnePageJunkieContestsFiltered(HttpSession session,
                                                      Model model,
                                                      @PathVariable int currentPage,
                                                      @PathVariable String title,
                                                      @PathVariable String category,
                                                      @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    title,
                    category,
                    "PHASEONE");

            Page<Contest> page = contestService.findPage(currentPage, contestFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            return "DashboardViewJunkie";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie")
    public String getAllPagesJunkieContests(HttpSession session,
                                          Model model,
                                          @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        return getOnePageJunkieContests(session, model, 1, contestFilterDto);
    }

    @GetMapping("/junkie/finished/page/{currentPage}")
    public String getOnePageJunkieFinishedContests(HttpSession session,
                                              Model model,
                                              @PathVariable int currentPage,
                                              @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategory(),
                    "FINAL");

            Page<Contest> page = contestService.findPageForParticipant(currentPage, contestFilterOptions, currentUser.getId());
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("title", contestFilterDto.getTitle());
            model.addAttribute("category", contestFilterDto.getCategory());
            return "DashboardViewJunkieFinished";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie/finished/page/{currentPage}/{title}&{category}")
    public String getOnePageJunkieFinishedContestsFiltered(HttpSession session,
                                                      Model model,
                                                      @PathVariable int currentPage,
                                                      @PathVariable String title,
                                                      @PathVariable String category,
                                                      @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    title,
                    category,
                    "FINAL");

            Page<Contest> page = contestService.findPageForParticipant(currentPage, contestFilterOptions, currentUser.getId());
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            return "DashboardViewJunkieFinished";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie/finished")
    public String getAllPagesJunkieFinishedContests(HttpSession session,
                                          Model model,
                                          @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        return getOnePageJunkieFinishedContests(session, model, 1, contestFilterDto);
    }

    @GetMapping("/junkie/ongoing/page/{currentPage}")
    public String getOnePageJunkieOngoingContests(HttpSession session,
                                              Model model,
                                              @PathVariable int currentPage,
                                              @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategory(),
                    "PHASEONE");

            Page<Contest> page = contestService.findPageForParticipant(currentPage, contestFilterOptions, currentUser.getId());
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            model.addAttribute("title", contestFilterDto.getTitle());
            model.addAttribute("category", contestFilterDto.getCategory());
            return "DashboardViewJunkieOngoing";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie/ongoing/page/{currentPage}/{title}&{category}")
    public String getOnePageJunkieOngoingContestsFiltered(HttpSession session,
                                                      Model model,
                                                      @PathVariable int currentPage,
                                                      @PathVariable String title,
                                                      @PathVariable String category,
                                                      @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if(currentUser.isOrganiser()){
                throw new AuthorizationException(ERROR_MESSAGE);
            }
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    title,
                    category,
                    "PHASEONE");

            Page<Contest> page = contestService.findPageForParticipant(currentPage, contestFilterOptions, currentUser.getId());
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Contest> contests = page.getContent();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("contests", contests);
            return "DashboardViewJunkieOngoing";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }
    @GetMapping("/junkie/ongoing")
    public String getAllPagesJunkieOngoingContests(HttpSession session,
                                          Model model,
                                          @ModelAttribute("contestFilterOptions") ContestFilterDto contestFilterDto) {
        return getOnePageJunkieOngoingContests(session, model, 1, contestFilterDto);
    }
    @GetMapping
    public String redirecter(HttpSession session,
                             Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        return currentUser.isOrganiser() ? "redirect:/my/organiser" : "redirect:/my/junkie";
    }
}
