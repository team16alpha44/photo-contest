package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.UserFilterDto;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Controller
@RequestMapping("/contests/{contestId}/jury")
public class AddJuryMvcController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this operation.";
    public static final String NOT_ELIGIBLE_FOR_JURY = "User does not have enough points accumulated to be part of the jury.";
    public static final String ALREADY_PARTICIPANT = "User is already a participant. Cannot make part of jury.";

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestService contestService;
    private final ProfilePictureService profilePictureService;
    private final PhotoService photoService;

    @Autowired
    public AddJuryMvcController(AuthenticationHelper authenticationHelper, UserService userService, ContestService contestService, ProfilePictureService profilePictureService, PhotoService photoService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.contestService = contestService;
        this.profilePictureService = profilePictureService;
        this.photoService = photoService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/eligible/page/{currentPage}")
    public String getOnePage(HttpSession session,
                             Model model,
                             @PathVariable int contestId,
                             @PathVariable int currentPage,
                             @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            checkIfOrganiser(currentUser);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            UserFilterOptions userFilterOptions = new UserFilterOptions(
                    userFilterDto.getUsername(),
                    userFilterDto.getFirstName(),
                    userFilterDto.getLastName(),
                    userFilterDto.getMinPoints(),
                    userFilterDto.getMaxPoints(),
                    userFilterDto.getSortBy(),
                    userFilterDto.getSortOrder(),
                    userFilterDto.getOrganiser());

            Page<User> page = userService.findPageAddJury(currentPage, userFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<User> users = page.getContent();
            Contest contest = contestService.getById(contestId);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("userFilterOptions", userFilterDto);
            model.addAttribute("users", users);
            model.addAttribute("contest", contest);
            model.addAttribute("username", userFilterDto.getUsername());
            model.addAttribute("lastName", userFilterDto.getLastName());
            model.addAttribute("points", userFilterDto.getMinPoints());
            return "AddJuryToContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/eligible/page/{currentPage}/{username}&{lastName}&{points}")
    public String getOnePageFiltered(HttpSession session,
                                     Model model,
                                     @PathVariable int contestId,
                                     @PathVariable int currentPage,
                                     @PathVariable String username,
                                     @PathVariable String lastName,
                                     @PathVariable String points,
                                     @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            checkIfOrganiser(currentUser);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        try {
            if (points.isEmpty()) {
                points = "0";
            }
            UserFilterOptions userFilterOptions = new UserFilterOptions(
                    username,
                    userFilterDto.getFirstName(),
                    lastName,
                    Integer.valueOf(points),
                    userFilterDto.getMaxPoints(),
                    userFilterDto.getSortBy(),
                    userFilterDto.getSortOrder(),
                    userFilterDto.getOrganiser());

            Page<User> page = userService.findPageAddJury(currentPage, userFilterOptions);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<User> users = page.getContent();
            Contest contest = contestService.getById(contestId);

            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("userFilterOptions", userFilterDto);
            model.addAttribute("users", users);
            model.addAttribute("contest", contest);
            return "AddJuryToContestView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/eligible")
    public String getAllPages(HttpSession session,
                              Model model,
                              @PathVariable int contestId,
                              @ModelAttribute("userFilterOptions") UserFilterDto userFilterDto) {
        return getOnePage(session, model, contestId, 1, userFilterDto);
    }

    @GetMapping("/eligible/{userId}")
    public String addJuryToContest(HttpSession session,
                                   Model model,
                                   @PathVariable int contestId,
                                   @PathVariable int userId) {
        User currentUser;
        User jurorToAdd;
        Contest contest;
        try {
            jurorToAdd = userService.getById(userId);
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            contest = contestService.getById(contestId);
            checkIfOrganiser(currentUser);
            model.addAttribute("currentUser", currentUser);
        } catch (EntityNotFoundException | AuthorizationException e) {
            return "error-404";
        }

        try {
            checkIfParticipant(photoService, contest, jurorToAdd);
            checkIfEligibleForJury(jurorToAdd);
            contest = contestService.getById(contestId);
            contest.addJuryToContest(jurorToAdd);
            contestService.update(contest);
            return "redirect:/contests/{contestId}/jury/eligible";
        } catch (EntityNotFoundException | DuplicateEntityException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    private static void checkIfParticipant(PhotoService photoService, Contest contest, User executingUser) {
        Stream<Photo> contestPhotos =
                contest.getPhotos().stream()
                        .map(x -> photoService.getById(x.getPhotoId()));
        if (contestPhotos.map(Photo::getAuthor).anyMatch(Predicate.isEqual(executingUser))) {
            throw new AuthorizationException(ALREADY_PARTICIPANT);
        }
    }

    private static void checkIfOrganiser(User currentUser) {
        if (!currentUser.isOrganiser()) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static void checkIfEligibleForJury(User targetUser) {
        if (targetUser.getPoints() < 151) {
            throw new AuthorizationException(NOT_ELIGIBLE_FOR_JURY);
        }
    }
}
