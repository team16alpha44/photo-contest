package com.company.photocontest.controllers.mvc;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final UserService userService;
    private final ProfilePictureService profilePictureService;


    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, ContestService contestService, UserService userService, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.userService = userService;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("latestPhotosCarousel")
    public List<Photo> populatePhotosForCarousel() {
        return contestService.getByPhase(Phase.FINAL).
                stream().
                limit(6).
                map(Contest::getId).map(contestService::getNumberOne).
                collect(Collectors.toList());
    }

    @ModelAttribute("openContests")
    public List<Contest> populateOpenContests() {
        return contestService.getByPhaseOneOpen().
                stream().
                limit(4).collect(Collectors.toList());
    }

    @GetMapping
    public String showHomePage(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            return "IndexView";
        }
        return "IndexView";
    }
}
