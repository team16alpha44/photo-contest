package com.company.photocontest.controllers.mvc;

import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ProfilePictureMapper;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.ProfilePictureDto;
import com.company.photocontest.services.contracts.ProfilePictureService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@Controller
@RequestMapping("/users/{userId}/profile-pictures")
public class ProfilePictureMvcController {

    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    private final AuthenticationHelper authenticationHelper;
    private final ProfilePictureMapper profilePictureMapper;
    private final ProfilePictureService profilePictureService;
    private final UserService userService;

    public ProfilePictureMvcController(AuthenticationHelper authenticationHelper, ProfilePictureMapper profilePictureMapper, ProfilePictureService profilePictureService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.profilePictureMapper = profilePictureMapper;
        this.profilePictureService = profilePictureService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showProfilePictures(@PathVariable int userId, HttpSession session, Model model){
        User currentUser=null;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        User user = userService.getById(userId);
        ProfilePicture profilePicture = profilePictureService.getCurrent(user);
        List<ProfilePicture> allPictures = profilePictureService.getByUser(user);
        model.addAttribute("profilePicture", profilePicture);
        model.addAttribute("user", user);
        model.addAttribute("allPictures", allPictures);

        return "ProfilePicturesAllView";
    }
    @PostMapping("/new")
    public String newPhoto(HttpSession session, Model model,
                           @Valid @ModelAttribute("dto") ProfilePictureDto dto,
                           @RequestPart("image") MultipartFile image,
                           BindingResult bindingResult,
                           @PathVariable int userId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }

        if (bindingResult.hasErrors()) {
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            return "ProfilePictureNewView";
        }

        try {
            if (image.isEmpty()) {
                model.addAttribute("error", "Please select a file to upload.");
                return "error-404";
            }
            ProfilePicture profilePicture = profilePictureMapper.dtoToProfilePicture(dto);
            User targetUser = userService.getById(userId);
            checkPermissions(userId, currentUser);
            profilePictureService.create(profilePicture, image, targetUser);
            return String.format("redirect:/users/%d/profile-pictures", userId);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/new")
    public String showUploadNew(@PathVariable int userId, HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);

        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            ProfilePictureDto dto = new ProfilePictureDto();
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("dto", dto);
            return "ProfilePictureNewView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/{pictureId}/select")
    public String selectProfilePicture(@PathVariable int userId, @PathVariable int pictureId, HttpSession session, Model model){
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);

        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            checkPermissions(userId, currentUser);
            ProfilePicture profilePicture = profilePictureService.getById(pictureId);
            profilePicture.setActive(true);
            profilePictureService.update(profilePicture);
            model.addAttribute("profilePicture", profilePicture);
            return "redirect:/users/{userId}";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    private static void checkPermissions(int targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }
}
