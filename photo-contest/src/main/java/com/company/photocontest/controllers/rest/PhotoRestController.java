package com.company.photocontest.controllers.rest;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.PhotoMapper;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Photo;
import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.PhotoDto;
import com.company.photocontest.payload.FileResponse;
import com.company.photocontest.services.contracts.ContestService;
import com.company.photocontest.services.contracts.PhotoService;
import com.company.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/photos")
public class PhotoRestController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";
    public static final String ERROR_MESSAGE_JUROR = "You cannot add your photo to a contest you are a jury of.";
    public static final String ERROR_MESSAGE_ALREADY_PARTICIPATING = "You cannot add more than one photo to a contest.";
    public static final String
            CONTEST_PHASE_ONE_HAS_ALREADY_PASSED = "Contest Phase one has already passed.";
    public static final String PHOTO_ADDED_TO_CONTEST
            = "Photo with photoId %d added to contest with contestId %d. You cannot change the photo later.";
    public static final String PHOTO_REMOVED_FROM_CONTEST = "Photo with photoId %d removed from contest with contestId %d.";
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final PhotoMapper photoMapper;

    @Autowired
    public PhotoRestController(PhotoService photoService, PhotoMapper photoMapper,
                               UserService userService, ContestService contestService,
                               AuthenticationHelper authenticationHelper) {
        this.photoService = photoService;
        this.photoMapper = photoMapper;
        this.userService = userService;
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<FileResponse> getById(@RequestHeader HttpHeaders headers,
                                                @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo repositoryPhoto = photoService.getById(id);
            checkJunkiePermissions(repositoryPhoto.getAuthor().getId(), executingUser);
            return new ResponseEntity<>(new FileResponse(repositoryPhoto.getId(), repositoryPhoto.getTitle(),
                    repositoryPhoto.getStory(), "Image is successfuly sent."),
                    HttpStatus.OK);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{title}")
    public ResponseEntity<FileResponse> getByTitle(@RequestHeader HttpHeaders headers,
                                                   @PathVariable String title) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo repositoryPhoto = photoService.getByTitle(title);
            checkJunkiePermissions(repositoryPhoto.getAuthor().getId(), executingUser);
            return new ResponseEntity<>(new FileResponse(repositoryPhoto.getId(), repositoryPhoto.getTitle(),
                    repositoryPhoto.getStory(), "Image is successfully sent."),
                    HttpStatus.OK);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @GetMapping
    public List<ResponseEntity<FileResponse>> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkOrganiserPermissions(executingUser);
            List<Photo> allPhotos = photoService.getAll();
            List<ResponseEntity<FileResponse>> responseEntities = new ArrayList<>();
            allPhotos.forEach(x -> responseEntities.add(new ResponseEntity<>(
                    new FileResponse(
                            x.getId(),
                            x.getTitle(),
                            x.getStory(),
                            "Image is successfuly sent."),
                    HttpStatus.OK)));
            return responseEntities;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<FileResponse> create(@RequestHeader HttpHeaders headers,
                                               @Valid @RequestPart String title,
                                               @Valid @RequestPart String story,
                                               @RequestPart MultipartFile image) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo photo = photoMapper.dtoToPhoto(new PhotoDto(title, story));
            photoService.create(photo, image, executingUser);
            return new ResponseEntity<>(new FileResponse(photo.getId(), photo.getTitle(),
                    photo.getStory(), "Image is successfuly received"),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Photo update(@RequestHeader HttpHeaders headers,
                        @PathVariable int id,
                        @Valid @RequestBody PhotoDto photoDto) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo photoToUpdate = photoService.getById(id);
            checkJunkiePermissions(photoToUpdate.getAuthor().getId(), executingUser);
            String oldImageName = "\\" + photoToUpdate.getTitle() + ".jpg";
            photoMapper.dtoToUpdatePhoto(photoToUpdate, photoDto);
            photoService.update(photoToUpdate, oldImageName);
            return photoToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkJunkiePermissions(photoService.getById(id).getAuthor().getId(), executingUser);
            photoService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{photoId}/contests/{contestId}")
    public ResponseEntity addPhotoToContest(@RequestHeader HttpHeaders headers,
                                            @PathVariable int contestId,
                                            @PathVariable int photoId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo photoToAdd = photoService.getById(photoId);
            checkJunkiePermissions(photoToAdd.getAuthor().getId(), executingUser);
            Contest contest = contestService.getById(contestId);
            checkContestPermissions(contest, executingUser);
            checkJuryPermissions(contest, executingUser);
            checkContestPhaseAddPhotos(contest);
            contestService.addPhotoToContest(contestId, photoId);
            return new ResponseEntity<>(
                    String.format(PHOTO_ADDED_TO_CONTEST, photoId, contestId),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{photoId}/contests/{contestId}")
    public ResponseEntity deletePhotoFromContest(@RequestHeader HttpHeaders headers,
                                                 @PathVariable int contestId,
                                                 @PathVariable int photoId) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Photo photoToRemove = photoService.getById(photoId);
            checkJunkiePermissions(photoToRemove.getAuthor().getId(), executingUser);
            Contest contest = contestService.getById(contestId);
            checkContestPermissions(contest, executingUser);
            checkIfJuryThrows(contest, executingUser , photoToRemove);
            checkContestPhaseAddPhotos(contest);
            contestService.removePhotoFromContest(contestId, photoId);
            return new ResponseEntity<>(
                    String.format(PHOTO_REMOVED_FROM_CONTEST, photoId, contestId),
                    HttpStatus.OK);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private static void checkJunkiePermissions(int targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {
            if (!executingUser.isOrganiser())
                throw new AuthorizationException(ERROR_MESSAGE);
        }
    }



    private static void checkOrganiserPermissions(User executingUser) {
        if (!executingUser.isOrganiser()) {
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static void checkContestPhaseAddPhotos(Contest contest) {
        if (!contest.getPhase().equals(Phase.PHASEONE)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, CONTEST_PHASE_ONE_HAS_ALREADY_PASSED);
        }
    }

    private static void checkJuryPermissions(Contest contest, User executingUser) {
        if (contest.getJurors().stream()
                .anyMatch(x -> x.equals(executingUser))) {
            throw new AuthorizationException(ERROR_MESSAGE_JUROR);
        }
    }

    private static void checkIfJuryThrows(Contest contest, User executingUser, Photo photo) {
        if (!contest.getJurors().contains(executingUser) || executingUser.getId() == photo.getAuthor().getId() ){
            throw new AuthorizationException(ERROR_MESSAGE_JUROR);
        }
    }

    private void checkContestPermissions(Contest contest, User executingUser) {
         if (contest.getPhotos().stream()
                 .map(p->photoService.getById(p.getPhotoId()).getAuthor())
                 .anyMatch(u->u.equals(executingUser))) {
             throw new DuplicateEntityException(ERROR_MESSAGE_ALREADY_PARTICIPATING);
        }
//
//        if(!contest.isOpen()){
//            if(!contest.getPhase().equals(Phase.PHASEONE))
//                throw new AuthorizationException(ERROR_MESSAGE);
//        }
    }
}
