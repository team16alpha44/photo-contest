package com.company.photocontest.controllers.mvc;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.helpers.AuthenticationHelper;
import com.company.photocontest.helpers.ScoreMapper;
import com.company.photocontest.models.*;
import com.company.photocontest.models.dto.ScoreDto;
import com.company.photocontest.services.contracts.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;

import static com.company.photocontest.enums.Phase.PHASETWO;

@Controller
@RequestMapping("/contests/{contestId}/photos/{photoId}")
public class ScoreMvcController {
    public static final String ERROR_MESSAGE = "You are not authorized to execute this user operation.";

    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final UserService userService;
    private final PhotoService photoService;
    private final ScoreService scoreService;
    private final ScoreMapper scoreMapper;
    private final ProfilePictureService profilePictureService;

    @Autowired
    public ScoreMvcController(AuthenticationHelper authenticationHelper,
                              ContestService contestService,
                              UserService userService,
                              PhotoService photoService, ScoreService scoreService, ScoreMapper scoreMapper, ProfilePictureService profilePictureService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.userService = userService;
        this.photoService = photoService;
        this.scoreService = scoreService;
        this.scoreMapper = scoreMapper;
        this.profilePictureService = profilePictureService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isPhase2")
    public boolean populateIsPhase2(HttpSession session, @PathVariable int contestId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            Contest contest = contestService.getById(contestId);
            return contest.getPhase().equals(PHASETWO);
        } catch (AuthorizationException e) {
            return false;
        }
    }

    @ModelAttribute("isPhaseFinal")
    public boolean populateIsPhaseFinal(HttpSession session, @PathVariable int contestId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            Contest contest = contestService.getById(contestId);
            return contest.getPhase().equals(Phase.FINAL);
        } catch (AuthorizationException e) {
            return false;
        }
    }

    @ModelAttribute("isJury")
    public boolean populateIsJury(HttpSession session, @PathVariable int contestId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            Contest contest = contestService.getById(contestId);
            return contest.getJurors().contains(currentUser) || currentUser.isOrganiser();
        } catch (AuthorizationException e) {
            return false;
        }
    }

    @GetMapping
    public String showContestPhotoPage(HttpSession session, Model model,
                                       @PathVariable int photoId,
                                       @PathVariable int contestId) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            Contest contest = contestService.getById(contestId);
            checkIfOrganiserOrJuryOrFinalPhase(currentUser, contest);
            checkIfPhotoInContest(contest, photoId);
            Photo photo = photoService.getById(photoId);
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("scoreDto", new ScoreDto());
            model.addAttribute("contest", contest);
            model.addAttribute("photo", photo);
            model.addAttribute("scores", scoreService.getAll().stream()
                    .filter(x -> x.getPhoto().equals(photo))
                    .filter(x -> x.getContest().equals(contest))
                    .collect(Collectors.toSet()));
            model.addAttribute("profilePictureService", profilePictureService);
            return "ContestPhotoView";
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @PostMapping("/addScore")
    public String addScore(HttpSession session,
                           @PathVariable int contestId,
                           @PathVariable int photoId,
                           @Valid @ModelAttribute("scoreDto") ScoreDto scoreDto,
                           BindingResult bindingResult,
                           Model model) {

        User currentUser;
        Contest contest;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            contest = contestService.getById(contestId);
        } catch (AuthorizationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        if (bindingResult.hasErrors()) {
            ProfilePicture profilePicture = profilePictureService.getCurrent(currentUser);
            model.addAttribute("profilePicture", profilePicture);
            model.addAttribute("profilePictureService", profilePictureService);
            return "ContestPhotoView";
        }
        try {
            checkContestPhaseIfSecond(contest);
        } catch (IllegalStateException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        try {
            Score score = scoreMapper.dtoToScore(scoreDto);
            Photo photo = photoService.getById(photoId);
            scoreService.create(score, photo, contest, currentUser);
            return "redirect:/contests/{contestId}/photos/{photoId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
    }

    @GetMapping("/addScore")
    public String showAfterAddScore(HttpSession session,
                                    Model model,
                                    @PathVariable int contestId,
                                    @PathVariable int photoId) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-404";
        }
        return "redirect:/contests/{contestId}/photos/{photoId}";
    }

    private static void checkContestPhaseIfSecond(Contest contest) {
        if (!contest.getPhase().equals(PHASETWO)) {
            throw new IllegalStateException("Contest is not in phase two");
        }
    }

    private static void checkIfPhotoInContest(Contest contest, int photoId) {
        if (contest.getPhotos().stream().noneMatch(p -> p.getPhotoId() == photoId))
            throw new EntityNotFoundException("No such image in this contest.");
    }

    private static void checkIfOrganiserOrJuryOrFinalPhase(User executingUser, Contest contest) {
        if (!(executingUser.isOrganiser() || contest.getJurors().contains(executingUser))) {
            if (checkContestPhaseForJunkie(contest))
                return;
            throw new AuthorizationException(ERROR_MESSAGE);
        }
    }

    private static boolean checkContestPhaseForJunkie(Contest contest) {
        if (!contest.getPhase().equals(Phase.FINAL)) {
            throw new AuthorizationException("Contest is not finished yet.");
        }
        return true;
    }
}
