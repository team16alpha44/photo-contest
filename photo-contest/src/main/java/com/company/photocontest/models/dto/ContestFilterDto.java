package com.company.photocontest.models.dto;

public class ContestFilterDto {
    private String title;
    private String category;
    private String phase;



    public ContestFilterDto() {

    }

    public  ContestFilterDto(String title, String category, String phase) {
        this.title = title;
        this.category = category;
        this.phase = phase;

    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
