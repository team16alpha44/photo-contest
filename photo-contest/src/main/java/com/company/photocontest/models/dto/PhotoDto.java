package com.company.photocontest.models.dto;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.sql.Blob;

public class PhotoDto {
    @NotNull(message =  "Title cannot be empty.")
    @Size(min = 3, max = 64, message = "Title should be between 3 and 64 symbols.")
    private String title;
    @Size(max = 8192, message = "Story should have less than 8192 symbols.")
    private String story;

    public PhotoDto() {
    }
    public PhotoDto(String title, String story) {
        setTitle(title);
        setStory(story);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
