package com.company.photocontest.models;

import jakarta.persistence.*;

@Entity
@Table(name="profile_pictures")
public class ProfilePicture {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="profile_picture_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name="image")
    private String image;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "is_active")
    private boolean isActive;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean whetherIsActive) {
        isActive = whetherIsActive;
    }
}
