package com.company.photocontest.models.dto;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;

public class EditProfileDto {


    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 symbols")
    private String firstName;

    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 symbols")
    private String lastName;

    @Email
    private String email;

    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    public EditProfileDto() {
    }

    public EditProfileDto(String firstName, String lastName, String email, String oldPassword, String newPassword, String confirmPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
