package com.company.photocontest.models.dto;

import jakarta.validation.constraints.Size;


public class ContestDto {

    @Size(min = 3, max = 50, message = "Title should be between 3 and 30 symbols.")
    private String title;

     private String category ;
    private boolean isOpen;

    private long phaseOneDays;

    private long phaseTwoHours;


    public ContestDto() {
    }

    public String getTitle() {
        return title;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean getIsOpen() {
        return isOpen;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public void setIsOpen(boolean isOpen) {
       this.isOpen  = isOpen;
    }

    public void setPhaseOneDays(long phaseOneDays) {
        this.phaseOneDays = phaseOneDays;
    }

    public void setPhaseTwoHours(long phaseTwoHours) {
        this.phaseTwoHours = phaseTwoHours;
    }

    public long getPhaseOneDays() {
        return phaseOneDays;
    }

    public long getPhaseTwoHours() {
        return phaseTwoHours;
    }
}


