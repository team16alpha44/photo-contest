package com.company.photocontest.models.dto;

import jakarta.validation.constraints.NotNull;

public class CategoryDto {
    @NotNull
    private String name ;

    public CategoryDto( ) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
