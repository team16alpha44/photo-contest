package com.company.photocontest.models;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name="photos_contests")
public class PhotoContestRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_contest_id")
    private int photoContestId;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "photo_id")
    private int photoId;

    @Column(name = "result")
    private int result;

    public int getPhotoContestId() {
        return photoContestId;
    }

    public void setPhotoContestId(int userBeerId) {
        this.photoContestId = photoContestId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoContestRelation that = (PhotoContestRelation) o;
        return contestId == that.contestId && photoId == that.photoId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestId, photoId);
    }
}
