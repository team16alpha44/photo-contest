package com.company.photocontest.models;

import com.company.photocontest.enums.Phase;

import java.util.Optional;

public class ContestFilterOptions {
    private Optional<String> title;
    private Optional<String> category;
    private Optional<String> phase;

    public ContestFilterOptions() {
        this(null ,null,null );
    }

    public ContestFilterOptions(String title,String category,String phase) {
        this.title = Optional.ofNullable(title);
        this.category = Optional.ofNullable(category);
        this.phase = Optional.ofNullable(phase);

    }

    public Optional<String> getPhase() {
        return phase;
    }

    public Optional<String> getTitle() {
        return title;
    }

    public Optional<String> getCategory() {
        return category;
    }

    public void setTitle(String title) {
        this.title = Optional.ofNullable(title);
    }

    public void setCategory(String category) {
        this.category = Optional.ofNullable(category);
    }

    public void setPhase(String phase) {
        this.phase = Optional.ofNullable(phase);
    }
}
