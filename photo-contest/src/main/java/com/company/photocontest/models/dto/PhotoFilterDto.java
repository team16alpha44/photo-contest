package com.company.photocontest.models.dto;

import java.util.Optional;

public class PhotoFilterDto {
    private String title;
    private String story;
    private Integer authorId;
    private String sortBy;
    private String sortOrder;

    public PhotoFilterDto() {
    }

    public PhotoFilterDto(String title, String story, Integer authorId, String sortBy, String sortOrder) {
        this.title = title;
        this.story = story;
        this.authorId = authorId;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
