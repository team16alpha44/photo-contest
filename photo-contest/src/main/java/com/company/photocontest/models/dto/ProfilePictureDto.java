package com.company.photocontest.models.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ProfilePictureDto {

    @NotNull(message =  "Title cannot be empty.")
    @Size(min = 3, max = 64, message = "Title should be between 3 and 64 symbols.")
    private String title;

    public ProfilePictureDto() {
    }

    public ProfilePictureDto(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
