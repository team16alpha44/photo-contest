package com.company.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private int id;
    @NotNull
    @Column(name="username")
    private String username;
    @NotNull
    @Column(name="password")
    private String password;
    @NotNull
    @Column(name="email")
    private String email;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="points")
    private int points;
    @Column(name="isOrganiser")
    private boolean isOrganiser;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<ProfilePicture> profilePictures = new HashSet<ProfilePicture>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "jurors")
    private Set<Contest> contestsAsJury;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Set<ProfilePicture> getProfilePictures() {
        return profilePictures;
    }

    public int getPoints() {
        return points;
    }
    public String currentRanking(){
        if (points <= 50) {
            return "Junkie";
        } else if (points <= 150) {
            return "Enthusiast";
        } else if (points <= 1000) {
            return "Master";
        } else {
            return "Wise and Benevolent Photo Dictator";
        }
    }
    public String nextRanking(){
        if (points <= 50) {
            return String.format("%d points to Enthusiast", 50 - points);
        } else if (points <= 150) {
            return String.format("%d points to Master", 150 - points);
        } else if (points <= 1000) {
            return String.format("%d points to Wise and Benevolent Photo Dictator", 1000 - points);
        } else {
            return "You are already one of the best";
        }
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public boolean isOrganiser() {
        return isOrganiser;
    }

    public void setOrganiser(boolean whetherIsOrganiser) {
        isOrganiser = whetherIsOrganiser;
    }

    public Set<Contest> getContestsAsJury() {
        return contestsAsJury;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
