package com.company.photocontest.models.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ScoreDto {

    @NotNull(message =  "Score cannot be empty.")
    @Min(value=1, message = "Score cannot be lower than 1.")
    @Max(value=10, message = "Score cannot be higher than 10.")
    private int score;
    @NotNull(message =  "Comment cannot be empty.")
    @Size(min = 4, max = 64, message = "Title should be between 4 and 256 symbols.")
    private String comment;
//    private boolean wrongCategory;

    public ScoreDto() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    public boolean isWrongCategory() {
//        return wrongCategory;
//    }
//
//    public void setWrongCategory(boolean wrongCategory) {
//        this.wrongCategory = wrongCategory;
//    }
}
