package com.company.photocontest.models;

import java.util.Optional;

public class UserFilterOptions {

    private Optional<String> username;
    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<Integer> minPoints;
    private Optional<Integer> maxPoints;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;
    private Optional<Boolean> isOrganiser;

    public UserFilterOptions() {
        this(null, null, null, null, null, null, null, null);
    }

    public UserFilterOptions(String username,
                             String firstName,
                             String lastName,
                             Integer minPoints,
                             Integer maxPoints,
                             String sortBy,
                             String sortOrder, Boolean isOrganiser) {
        this.username = Optional.ofNullable(username);
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.minPoints = Optional.ofNullable(minPoints);
        this.maxPoints = Optional.ofNullable(maxPoints);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
        this.isOrganiser = Optional.ofNullable(isOrganiser);
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public Optional<Integer> getMinPoints() {
        return minPoints;
    }

    public Optional<Integer> getMaxPoints() {
        return maxPoints;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public Optional<Boolean> getIsOrganiser() {
        return isOrganiser;
    }
}
