package com.company.photocontest.models;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "scores")
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "score_id")
    public int id;
    @Column(name = "points")
    private int points;
    @ManyToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;
    @ManyToOne
    @JoinColumn(name = "juror_id")
    private User juror;
    @Column(name = "comment")
    private String comment ;

    @ManyToOne
    @JoinColumn(name="contest_id")
    private Contest contest;

    public Score() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return id == score.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
