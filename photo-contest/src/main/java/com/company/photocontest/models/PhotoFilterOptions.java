package com.company.photocontest.models;

import java.util.Optional;

public class PhotoFilterOptions {

    private Optional<String> title;
    private Optional<String> story;
    private Optional<Integer> authorId;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public PhotoFilterOptions() {
    }

    public PhotoFilterOptions(String title, String story, int authorId, String sortBy, String sortOrder) {
        this.title = Optional.ofNullable(title);
        this.story = Optional.ofNullable(story);
        this.authorId = Optional.of(authorId);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = Optional.ofNullable(title);
    }

    public Optional<String> getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = Optional.ofNullable(story);
    }

    public Optional<Integer> getAuthorId() {
        return authorId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
