package com.company.photocontest.models;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.AuthorizationException;
import com.company.photocontest.exceptions.DuplicateEntityException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.company.photocontest.enums.Phase.*;


@Entity
@Table(name = "contests")
public class Contest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    public int id;

    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "category_id")
    private Category category;

    @Enumerated(EnumType.STRING)
    @Column(name = "phase")
    private Phase phase;

    @Column(name = "is_open")
    private boolean isOpen;

    @ManyToOne
    @JoinColumn(name = "cover_photo_id")
    private Photo coverPhoto;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinTable(
            name = "jurors",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jurors;


    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<PhotoContestRelation> photos;

    public Contest() {
        createdDate = LocalDateTime.now();
        phase = PHASEONE;
        jurors = new HashSet<>();
        photos = new HashSet<>();
    }

    @Column(name = "created_date_time")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDateTime createdDate;
    @Column(name = "phase1_expired_time")
    private LocalDateTime expiredDatePhase1;
    @Column(name = "phase2_expired_time")
    private LocalDateTime expiredDatePhase2;


    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }


    public void setExpiredDatePhase1(LocalDateTime expiredDatePhase1) {
        this.expiredDatePhase1 = expiredDatePhase1;
    }

    public void setExpiredDatePhase2(LocalDateTime expiredDatePhase2) {
        this.expiredDatePhase2 = expiredDatePhase2;
    }


    public LocalDateTime getExpiredDatePhase1() {
        return expiredDatePhase1;
    }

    public LocalDateTime getExpiredDatePhase2() {
        return expiredDatePhase2;
    }

    public Set<User> getJurors() {
        return jurors;
    }

    public int getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public Category getCategory() {
        return category;
    }

    public Phase getPhase() {
        return phase;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public Photo getCoverPhoto() {
        return coverPhoto;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }


    public void setCoverPhoto(Photo coverPhoto) {
        this.coverPhoto = coverPhoto;
    }


    public Set<PhotoContestRelation> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<PhotoContestRelation> photos) {
        this.photos = photos;
    }


    public void addJuryToContest(User user) {
        if (this.getPhase() == FINAL) {
            throw new AuthorizationException("Contest has already finished!");
        }
        if(user.isOrganiser()){
            throw new DuplicateEntityException("User is a organiser already!");
        }
        jurors.add(user);
        user.getContestsAsJury().add(this);
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }


    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;

    }


    public void removeJuror(User user) {
        User jurorToRemove = this.jurors.stream().filter(t -> t.getId() == user.getId()).findFirst().orElse(null);
        if (jurorToRemove != null) {
            this.jurors.remove(jurorToRemove);
            jurorToRemove.getContestsAsJury().remove(this);
        }
    }

    public LocalDateTime nextPhaseDate() {
        return switch (getPhase()) {
            case PHASEONE -> getExpiredDatePhase1();
            case PHASETWO -> getExpiredDatePhase2();
            case FINAL -> LocalDateTime.now();
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return id == contest.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}