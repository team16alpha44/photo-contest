package com.company.photocontest.helpers;

import com.company.photocontest.models.Category;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.dto.CategoryDto;
import com.company.photocontest.models.dto.ContestDto;
import com.company.photocontest.services.contracts.CategoryService;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    private final CategoryService categoryService;

    public CategoryMapper(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    public Category dtoToCategory(int id, CategoryDto categoryDto){
        Category category = dtoToCategory(categoryDto);
        category.setId(id);
        Category repositoryContest = categoryService.getById(id);
        category.setName(repositoryContest.getName());
        return category;
    }
    public Category dtoToCategory(CategoryDto categoryDto){
        Category category = new Category();
        category.setName(categoryDto.getName());


        return category;
    }

}
