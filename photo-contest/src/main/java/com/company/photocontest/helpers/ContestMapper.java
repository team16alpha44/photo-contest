package com.company.photocontest.helpers;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.dto.ContestDto;
import com.company.photocontest.repositories.contracts.CategoryRepository;
import com.company.photocontest.services.contracts.CategoryService;
import com.company.photocontest.services.contracts.ContestService;
import org.springframework.stereotype.Component;

@Component
public class ContestMapper {
    private final CategoryService categoryService;

    public ContestMapper(CategoryService categoryService ) {
        this.categoryService = categoryService;

    }

    public Contest dtoToContest(int id, ContestDto contestDto) {
        Contest contest = dtoToContest(contestDto);
        contest.setId(id);
        return contest;
    }


    public Contest dtoToContest(ContestDto contestDto) {
        Contest contest = new Contest();
        categoryService.addCategoryToContest(contestDto.getCategory());
        contest.setCategory(categoryService.getByName(contestDto.getCategory()));
        contest.setTitle(contestDto.getTitle());
        contest.setIsOpen(contestDto.getIsOpen());
        contest.setExpiredDatePhase1(contest.getCreatedDate().plusDays(contestDto.getPhaseOneDays()));
        contest.setExpiredDatePhase2(contest.getCreatedDate().plusDays(contestDto.getPhaseOneDays())
                .plusHours(contestDto.getPhaseTwoHours()));

        return contest;
    }
}



