package com.company.photocontest.helpers;

import com.company.photocontest.models.ProfilePicture;

import com.company.photocontest.models.dto.ProfilePictureDto;

import org.springframework.stereotype.Component;

@Component
public class ProfilePictureMapper {

    public ProfilePicture dtoToProfilePicture(ProfilePictureDto profilePictureDto) {
        ProfilePicture profilePicture = new ProfilePicture();
        profilePicture.setTitle(profilePictureDto.getTitle());
        return profilePicture;
    }

}
