package com.company.photocontest.helpers;

import com.company.photocontest.models.Score;
import com.company.photocontest.models.dto.ScoreDto;
import com.company.photocontest.services.contracts.ScoreService;
import org.springframework.stereotype.Component;

@Component
public class ScoreMapper {
    private final ScoreService scoreService;

    public ScoreMapper(ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    public Score dtoToScore(int id, ScoreDto scoreDto) {
        Score score = dtoToScore(scoreDto);
        score.setId(id);
        Score repositoryScore = scoreService.getById(id);
        score.setJuror(repositoryScore.getJuror());
        score.setPhoto(repositoryScore.getPhoto());
        score.setContest(repositoryScore.getContest());
        return score;
    }

    public Score dtoToScore(ScoreDto scoreDto) {
        Score score = new Score();
        score.setComment(scoreDto.getComment());
//        if (scoreDto.isWrongCategory())
//            score.setPoints(0);
//        else score.setPoints(scoreDto.getScore());
        score.setPoints(scoreDto.getScore());
        return score;
    }

    public void dtoToUpdateScore(Score score, ScoreDto scoreDto) {
        score.setComment(scoreDto.getComment());
        score.setPoints(scoreDto.getScore());
    }
}
