package com.company.photocontest.helpers;

import com.company.photocontest.models.Photo;
import com.company.photocontest.models.dto.PhotoDto;
import com.company.photocontest.services.contracts.PhotoService;
import org.springframework.stereotype.Component;

@Component
public class PhotoMapper {
    private final PhotoService photoService;

    public PhotoMapper(PhotoService photoService) {
        this.photoService = photoService;
    }
    public Photo dtoToPhoto(int id, PhotoDto photoDto){
        Photo photo = dtoToPhoto(photoDto);
        photo.setId(id);
        Photo repositoryPhoto = photoService.getById(id);
        photo.setAuthor(repositoryPhoto.getAuthor());
        return photo;
    }
    public Photo dtoToPhoto(PhotoDto photoDto){
        Photo photo = new Photo();
        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
        return photo;
    }
    public void dtoToUpdatePhoto(Photo photo, PhotoDto photoDto){
        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
    }
}
