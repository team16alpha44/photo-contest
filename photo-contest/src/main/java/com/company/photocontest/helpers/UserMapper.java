package com.company.photocontest.helpers;

import com.company.photocontest.models.User;
import com.company.photocontest.models.dto.*;
import com.company.photocontest.services.contracts.UserService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;

    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User dtoToUser(int id, UserUpdateDto userUpdateDto) {
        User repositoryUser = userService.getById(id);
        repositoryUser.setFirstName(userUpdateDto.getFirstName());
        repositoryUser.setLastName(userUpdateDto.getLastName());
        repositoryUser.setEmail(userUpdateDto.getEmail());
        repositoryUser.setPassword(userUpdateDto.getPassword());
        return repositoryUser;
    }

    public UserOutDto userToOutDto(User user) {
        UserOutDto userOutDto = new UserOutDto();
        userOutDto.setId(user.getId());
        userOutDto.setFirstName(user.getFirstName());
        userOutDto.setLastName(user.getLastName());
        userOutDto.setUsername(user.getUsername());
        userOutDto.setEmail(user.getEmail());
        return userOutDto;
    }

    public User dtoToUser(RegisterDto register) {
        User user = new User();
        user.setFirstName(register.getFirstName());
        user.setLastName(register.getLastName());
        user.setUsername(register.getUsername());
        user.setPassword(register.getPassword());
        user.setEmail(register.getEmail());
        return user;
    }

    public User editProfileDtoToUser(EditProfileDto user, int id) {

        User repositoryUser = userService.getById(id);
        repositoryUser.setFirstName(user.getFirstName());
        repositoryUser.setLastName(user.getLastName());
        repositoryUser.setEmail(user.getEmail());
        if (user.getNewPassword() != null & !user.getNewPassword().isEmpty()) {
            repositoryUser.setPassword(BCrypt.hashpw(user.getNewPassword(), BCrypt.gensalt()));
        }
        return repositoryUser;
    }

    public EditProfileDto userToEditProfileDto(User user){
        EditProfileDto dto = new EditProfileDto();
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        return dto;
    }

}
