package com.company.photocontest.payload;

public class FileResponse {
    private int imageId;
    private String imageTitle;
    private String imageStory;
    private String message;

    public FileResponse(int imageId, String imageTitle, String message) {
        this.imageId = imageId;
        this.imageTitle = imageTitle;
        this.message = message;
    }

    public FileResponse(String imageTitle, String imageStory, String message) {
        this.imageTitle = imageTitle;
        this.imageStory = imageStory;
        this.message = message;
    }

    public FileResponse(int imageId, String imageTitle, String imageStory, String message) {
        this.imageId = imageId;
        this.imageTitle = imageTitle;
        this.imageStory = imageStory;
        this.message = message;
    }


    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageStory() {
        return imageStory;
    }

    public void setImageStory(String imageStory) {
        this.imageStory = imageStory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
