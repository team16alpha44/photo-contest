package com.company.photocontest.repositories;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import com.company.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public void create(User entity) {
        entity.setPassword(BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt()));
        super.create(entity);
    }

    @Override
    public User getByUsername(String username) {
        return super.getByField("username", username);
    }

    @Override
    public User getByEmail(String email) {
        return super.getByField("email", email);
    }

    @Override
    public List<User> get(UserFilterOptions userFilterOptions) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            return generateQuery(session, userFilterOptions, Optional.empty()).list();
        }
    }

    private String generateOrderBy(UserFilterOptions userFilterOptions) {
        if (userFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (userFilterOptions.getSortBy().get().toLowerCase()) {
            case "username" -> orderBy = "username";
            case "firstname" -> orderBy = "firstName";
            case "lastname" -> orderBy = "lastName";
            case "points" -> orderBy = "points";
            default -> {
                return "";
            }
        }
        orderBy = String.format(" order by %s", orderBy);

        if (userFilterOptions.getSortOrder().isPresent() &&
                userFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

    @Override
    public Page<User> getAllAddJury(Pageable pageable, UserFilterOptions userFilterOptions) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<User> query = generateQuery(session, userFilterOptions, Optional.empty());
            return pageable.isUnpaged() ?
                    new PageImpl<>(query.list()) :
                    readPage(query, pageable, query.list().size());
        }
    }
    @Override
    public Page<User> getAllJuryInContest(Pageable pageable, UserFilterOptions userFilterOptions, Contest contest) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<User> query = generateQuery(session, userFilterOptions, Optional.of(contest));
            List<User> users = query.list();
            return pageable.isUnpaged() ?
                    new PageImpl<>(users) :
                    readPage(query, pageable, users.size());
        }
    }

    public Query<User> generateQuery(Session session, UserFilterOptions userFilterOptions, Optional<Contest> contest){
        StringBuilder queryString = new StringBuilder(" from User u ");
        Map<String, Object> queryParams = new HashMap<>();
        List<String> filter = new ArrayList<>();
        userFilterOptions.getUsername().ifPresent(v -> {
            filter.add(" username like :username ");
            queryParams.put("username", "%" + v + "%");
        });
        userFilterOptions.getFirstName().ifPresent(v -> {
            filter.add(" firstName like :firstName ");
            queryParams.put("firstName", "%" + v + "%");
        });
        userFilterOptions.getLastName().ifPresent(v -> {
            filter.add(" lastName like :lastName ");
            queryParams.put("lastName", "%" + v + "%");
        });
        userFilterOptions.getMinPoints().ifPresent(v -> {
            filter.add(" points >= :minPoints ");
            queryParams.put("minPoints", v);
        });
        userFilterOptions.getMaxPoints().ifPresent(v -> {
            filter.add(" points <= :maxPoints ");
            queryParams.put("maxPoints", v);
        });
        userFilterOptions.getIsOrganiser().ifPresent(v->{
            filter.add(" isOrganiser = :isOrganiser ");
            queryParams.put("isOrganiser", v);
        });

        if (contest.isPresent()) {
            queryString.append("inner join u.contestsAsJury c on c.id = :contestId ");
        }

        if (!filter.isEmpty()) {
            queryString.append(" where ").append(String.join(" and ", filter));
        }

        queryString.append(generateOrderBy(userFilterOptions));

        Query<User> userQuery = session.createQuery(queryString.toString(), User.class);
        contest.ifPresent(v -> userQuery.setParameter("contestId", v.getId()));
        return userQuery.setProperties(queryParams);
    }
}
