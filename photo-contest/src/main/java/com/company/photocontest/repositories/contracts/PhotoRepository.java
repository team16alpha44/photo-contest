package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.Photo;
import com.company.photocontest.models.PhotoFilterOptions;
import com.company.photocontest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PhotoRepository extends BaseCRUDRepository<Photo>, PagingAndSortingRepository<Photo, Integer> {

    Photo getByTitle(String title);

    List<Photo> getByAuthor(User author);

    Page<Photo> getByPagination(Pageable pageable, PhotoFilterOptions photoFilterOptions);

    Page<Photo> findAll(Pageable pageable);
}
