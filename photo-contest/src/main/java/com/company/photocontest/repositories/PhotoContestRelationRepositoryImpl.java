package com.company.photocontest.repositories;

import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.PhotoContestRelation;
import com.company.photocontest.models.Score;
import com.company.photocontest.repositories.contracts.PhotoContestRelationRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoContestRelationRepositoryImpl extends AbstractCRUDRepository<PhotoContestRelation> implements PhotoContestRelationRepository {
    @Autowired
    public PhotoContestRelationRepositoryImpl(SessionFactory sessionFactory) {
        super(PhotoContestRelation.class, sessionFactory);
    }


    public PhotoContestRelation get(int contestId, int photoId) {
        try (Session session = sessionFactory.openSession()) {
            List<PhotoContestRelation> photoContestRelation = session
                    .createQuery("from PhotoContestRelation where contestId = :contestId and photoId = :photoId"
                            , PhotoContestRelation.class)
                    .setParameter("contestId", contestId)
                    .setParameter("photoId", photoId)
                    .list();

            if (photoContestRelation.isEmpty()) {
                throw new EntityNotFoundException("PhotoContestRelation", "contestId", String.valueOf(contestId));
            }
            return photoContestRelation.get(0);
        }
    }

    public List<PhotoContestRelation> getSortedByResult(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from PhotoContestRelation where contestId = :contestId order by result desc"
                            , PhotoContestRelation.class)
                    .setParameter("contestId", contestId)
                    .list();
        }
    }

    @Override
    public void delete(int contestId, int photoId) {
        PhotoContestRelation photoContestRelation = get(contestId, photoId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(photoContestRelation);
            session.getTransaction().commit();
        }
    }
}
