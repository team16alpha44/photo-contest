package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;

import java.util.List;


public interface ProfilePictureRepository extends BaseCRUDRepository<ProfilePicture>{

    ProfilePicture getByTitle(String title);
    List<ProfilePicture> getByUser(User user);

    ProfilePicture getCurrent(User user);
}
