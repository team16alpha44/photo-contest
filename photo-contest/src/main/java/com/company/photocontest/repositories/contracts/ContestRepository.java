package com.company.photocontest.repositories.contracts;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.ContestFilterOptions;
import com.company.photocontest.models.Photo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    Contest getByTitle(String title);

    List<Contest> get(ContestFilterOptions contestFilterOptions);

    List<Contest> getByCategory(String category);

    List<Contest> getByPhase(Phase phase);

    List<Contest> getByPhaseOneOpen();

    Page<Contest> getByPagination(Pageable pageable, ContestFilterOptions contestFilterOptions);

    List<Contest> getByParticipant(int userId);

    Page<Contest> getByParticipantPagination(Pageable pageable, ContestFilterOptions contestFilterOptions, int participantId);

}
