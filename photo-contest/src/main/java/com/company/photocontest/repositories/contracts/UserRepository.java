package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.User;
import com.company.photocontest.models.UserFilterOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> get(UserFilterOptions userFilterOptions);

    User getByUsername(String username);

    User getByEmail(String email);

    Page<User> getAllAddJury(Pageable pageable, UserFilterOptions userFilterOptions);
    Page<User> getAllJuryInContest(Pageable pageable, UserFilterOptions userFilterOptions, Contest contest);
}
