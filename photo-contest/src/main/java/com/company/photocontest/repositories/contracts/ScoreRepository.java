package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Score;
import com.company.photocontest.models.User;

import java.util.List;

public interface ScoreRepository extends BaseCRUDRepository<Score> {
    List<Score> getByJuror(User juror);

    List<Score> getByContest(Contest contest);
}
