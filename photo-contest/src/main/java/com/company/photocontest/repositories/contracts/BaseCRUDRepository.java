package com.company.photocontest.repositories.contracts;

import java.io.IOException;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T>{

    void delete(int id);

    void create(T entity);

    void update(T entity);
}
