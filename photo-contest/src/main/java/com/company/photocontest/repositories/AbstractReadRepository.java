package com.company.photocontest.repositories;


import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.util.List;

public abstract class AbstractReadRepository<T> extends SimpleJpaRepository<T, Integer> implements BaseReadRepository<T> {
    private final Class<T> clazz;
    protected SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory.openSession());
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s", clazz.getName()), clazz).list();
        }
    }

    public T getById(int id) {
        return getByField("id", id);
    }

    public <V> T getByField(String name, V value) {
        String query = String.format("from %s where %s = :value", clazz.getName(), name);
        String notFoundErrorMessage = String.format("%s with %s %s not found", clazz.getSimpleName(), name, value);

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() ->
                            new EntityNotFoundException(notFoundErrorMessage));
        }
    }

    public <V> List<T> getByFieldList(String name, V value) {
        String query = String.format("from %s where %s = :value", clazz.getName(), name);

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .list();
        }
    }
}
