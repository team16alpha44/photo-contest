package com.company.photocontest.repositories;

import com.company.photocontest.models.Contest;
import com.company.photocontest.models.Score;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.ScoreRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ScoreRepositoryImpl extends AbstractCRUDRepository<Score> implements ScoreRepository {
    @Autowired
    public ScoreRepositoryImpl(SessionFactory sessionFactory) {
        super(Score.class, sessionFactory);
    }

    @Override
    public List<Score> getByJuror(User juror) {
        return super.getByFieldList("juror", juror);
    }

    public List<Score> getByContest(Contest contest){
        return super.getByFieldList("contest", contest);
    }


}
