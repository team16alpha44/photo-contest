package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.PhotoContestRelation;

import java.util.List;

public interface PhotoContestRelationRepository extends BaseCRUDRepository<PhotoContestRelation> {

    PhotoContestRelation get(int contestId, int photoId);
    List<PhotoContestRelation> getSortedByResult(int contestId);
    void delete(int contestId, int photoId);
}
