package com.company.photocontest.repositories;

import com.company.photocontest.models.Category;
import com.company.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {
    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }

    @Override
    public List<Category> getAll() {
        return super.getAll();
    }

    @Override
    public Category getById(int id) {
        return super.getByField("category_id", id);
    }

    @Override
    public Category getByName(String name){
        return super.getByField("name", name);
    }



}
