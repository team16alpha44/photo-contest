package com.company.photocontest.repositories;

import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.PhotoContestRelation;
import com.company.photocontest.models.ProfilePicture;
import com.company.photocontest.models.User;
import com.company.photocontest.repositories.contracts.ProfilePictureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Repository
public class ProfilePictureRepositoryImpl extends AbstractCRUDRepository<ProfilePicture> implements ProfilePictureRepository {

    private static final String rootPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";
    private static final String PROFILE_SUFFIX = "\\profilePictures\\";
    @Autowired
    public ProfilePictureRepositoryImpl(SessionFactory sessionFactory) {
        super(ProfilePicture.class, sessionFactory);
    }

    @Override
    public void delete(int id) {
        ProfilePicture toDelete = getById(id);
        try {
            Files.deleteIfExists(Paths.get(rootPath + toDelete.getUser().getUsername() + PROFILE_SUFFIX + toDelete.getTitle() + ".jpg"));
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(toDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public ProfilePicture getByTitle(String title) {
        return super.getByField("title", title);
    }

    @Override
    public List<ProfilePicture> getByUser(User user) {
        return super.getByFieldList("user", user);
    }

    public ProfilePicture getCurrent(User user) {
        try (Session session = sessionFactory.openSession()) {
            List<ProfilePicture> profilePictures = session
                    .createQuery("from ProfilePicture pp where user = :user and isActive = true"
                            , ProfilePicture.class)
                    .setParameter("user", user)
                    .list();

            if (profilePictures.isEmpty()) {
                throw new EntityNotFoundException("ProfilePicture", "status", "'is active'", "user");
            }
            return profilePictures.get(0);
        }
    }
}

