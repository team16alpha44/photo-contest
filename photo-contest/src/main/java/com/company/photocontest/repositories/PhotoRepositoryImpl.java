package com.company.photocontest.repositories;

import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.*;
import com.company.photocontest.repositories.contracts.PhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Repository
public class PhotoRepositoryImpl extends AbstractCRUDRepository<Photo> implements PhotoRepository {
    private static final String getPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
    }

    @Override
    public Photo getByTitle(String title) {
        return super.getByField("title", title);
    }

    @Override
    public List<Photo> getByAuthor(User author) {
        return super.getByFieldList("author", author);
    }

    @Override
    public Page<Photo> getByPagination(Pageable pageable, PhotoFilterOptions photoFilterOptions) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<Photo> query = generateQuery(session, photoFilterOptions);
            List<Photo> photos = query.list();
            return pageable.isUnpaged() ?
                    new PageImpl<>(photos) :
                    readPage(query, pageable, photos.size());
        }
    }



    @Override
    public void delete(int id) {
        Photo toDelete = getById(id);
        try {
            Files.deleteIfExists(Paths.get(getPath + toDelete.getAuthor().getUsername() + '\\' + toDelete.getTitle() + ".jpg"));
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(toDelete);
            session.getTransaction().commit();
        }
    }
    private String generateOrderBy(PhotoFilterOptions photoFilterOptions) {
        if (photoFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (photoFilterOptions.getSortBy().get().toLowerCase()) {
            case "username" -> orderBy = "username";
            case "firstname" -> orderBy = "firstName";
            case "lastname" -> orderBy = "lastName";
            case "points" -> orderBy = "points";
            default -> {
                return "";
            }
        }
        orderBy = String.format(" order by %s", orderBy);

        if (photoFilterOptions.getSortOrder().isPresent() &&
                photoFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }
    public Query<Photo> generateQuery(Session session, PhotoFilterOptions photoFilterOptions){
        StringBuilder queryString = new StringBuilder(" from Photo ");
        Map<String, Object> queryParams = new HashMap<>();
        List<String> filter = new ArrayList<>();
        photoFilterOptions.getTitle().ifPresent(v -> {
            filter.add(" title like :title ");
            queryParams.put("title", "%" + v + "%");
        });
        photoFilterOptions.getStory().ifPresent(v -> {
            filter.add(" story like :story ");
            queryParams.put("story", "%" + v + "%");
        });
        photoFilterOptions.getAuthorId().ifPresent(v -> {
            filter.add(" author.id = :authorId ");
            queryParams.put("authorId", v);
        });

        if (!filter.isEmpty()) {
            queryString.append(" where ").append(String.join(" and ", filter));
        }

        queryString.append(generateOrderBy(photoFilterOptions));

        Query<Photo> userQuery = session.createQuery(queryString.toString(), Photo.class);
        return userQuery.setProperties(queryParams);
    }
}
