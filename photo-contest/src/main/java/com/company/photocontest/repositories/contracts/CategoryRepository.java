package com.company.photocontest.repositories.contracts;

import com.company.photocontest.models.Category;
import com.company.photocontest.repositories.contracts.BaseReadRepository;

import java.util.List;

public interface CategoryRepository extends BaseCRUDRepository<Category> {
    @Override
    List<Category> getAll();

    @Override
    Category getById(int id);

    Category getByName(String name);
}
