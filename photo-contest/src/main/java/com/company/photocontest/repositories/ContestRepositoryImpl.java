package com.company.photocontest.repositories;

import com.company.photocontest.enums.Phase;
import com.company.photocontest.exceptions.EntityNotFoundException;
import com.company.photocontest.models.Contest;
import com.company.photocontest.models.ContestFilterOptions;
import com.company.photocontest.models.User;
import com.company.photocontest.models.Photo;
import com.company.photocontest.repositories.contracts.CategoryRepository;
import com.company.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.company.photocontest.enums.Phase.PHASEONE;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    private static final String getPath = "photo-contest\\src\\main\\resources\\static\\photoUpload\\";

    private final CategoryRepository categoryRepository;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory, CategoryRepository categoryRepository) {
        super(Contest.class, sessionFactory);
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Contest getByTitle(String title) {
        return super.getByField("title", title);

    }

    @Override
    public List<Contest> get(ContestFilterOptions contestFilterOptions) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            return generateQuery(session, contestFilterOptions).list();
        }
    }


    @Override
    public List<Contest> getByCategory(String category) {
        return super.getByFieldList("category", category);
    }

    @Override
    public List<Contest> getByPhase(Phase phase) {
        return super.getByFieldList("phase", phase);
    }

    @Override
    public Page<Contest> getByPagination(Pageable pageable, ContestFilterOptions contestFilterOptions) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<Contest> query = generateQuery(session, contestFilterOptions);
            List<Contest> contests = query.list();
            return pageable.isUnpaged() ?
                    new PageImpl<>(contests) :
                    readPage(query, pageable, contests.size());
        }
    }

    @Override
    public Page<Contest> getByParticipantPagination(Pageable pageable, ContestFilterOptions contestFilterOptions, int participantId) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<Contest> query = session
                    .createQuery("from Contest c " +
                                "left join c.category cat " +
                                "left join c.photos pr " +
                                "inner join Photo ph on ph.id = pr.photoId " +
                                "inner join User on ph.author.id = :authorId " +
                                "where c.title like :title and cat.name like :category and c.phase = :phase", Contest.class);
            query.setParameter("authorId", participantId);
            contestFilterOptions.getTitle().ifPresent(v -> {
                query.setParameter("title", "%" + v + "%");
            });
            contestFilterOptions.getCategory().ifPresent(v -> {
                query.setParameter("category", "%" + v + "%");
            });
            contestFilterOptions.getPhase().ifPresent(v -> {
                query.setParameter("phase", Phase.valueOf(v));
            });
            List<Contest> contests = query.list();
            return pageable.isUnpaged() ?
                    new PageImpl<>(contests) :
                    readPage(query, pageable, contests.size());
        }
    }

    public List<Contest> getByParticipant(int userId) {
        try (
                Session session = sessionFactory.openSession()
        ) {
            Query<Contest> query = session
                    .createQuery("from Contest c " +
                            "left join c.photos pr " +
                            "inner join Photo ph on ph.id = pr.photoId " +
                            "inner join User on ph.author.id = :authorId ", Contest.class);
            query.setParameter("authorId", userId);
            return query.list();
        }
    }



    private Query<Contest> generateQuery(Session session, ContestFilterOptions contestFilterOptions) {
        StringBuilder queryString = new StringBuilder(" from Contest c left join c.category cat");
        Map<String, Object> queryParams = new HashMap<>();
        List<String> filter = new ArrayList<>();
        final Phase[] phase = new Phase[1];

        contestFilterOptions.getTitle().ifPresent(v -> {
            filter.add(" title like :title ");
            queryParams.put("title", "%" + v + "%");
        });


        contestFilterOptions.getCategory().ifPresent(v -> {
            filter.add(" category.name like :category ");
            queryParams.put("category", "%" + v + "%");
        });


        if (!filter.isEmpty()) {
            queryString.append(" where ").append(String.join(" and ", filter));
        }
        contestFilterOptions.getPhase().ifPresent(v -> {
            if (!v.equals("")) {
                queryString.append(" and phase = :phase");
                phase[0] = Phase.valueOf(v);
            }
        });
        Query<Contest> contestQuery = session.createQuery(queryString.toString(), Contest.class);
        contestQuery.setProperties(queryParams);
        contestFilterOptions.getPhase().ifPresent(v -> {
            if (!v.equals(""))
                contestQuery.setParameter("phase", phase[0]);
        });
        return contestQuery;


    }


public void updateContestPhoto(Contest contest , Photo photo){
    try (Session session = sessionFactory.openSession()) {
        session.beginTransaction();
        session.merge(contest);
        session.persist(photo);
        session.getTransaction().commit();
    }
}





    public Page<Contest> getByPhasePagination(Phase phase, Pageable pageable) {
        return new PageImpl<>(getByPhase(phase));
    }

    @Override
    public List<Contest> getByPhaseOneOpen() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Contest where phase = :phase and isOpen = :isOpen", Contest.class)
                    .setParameter("phase", PHASEONE)
                    .setParameter("isOpen", true)
                    .list();
        }
    }
}
