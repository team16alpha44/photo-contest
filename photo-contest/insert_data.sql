
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `password`, `email`, `points`, `isOrganiser`)
VALUES (1, 'TESTUSERNAME', 'TESTUSERFNAME', 'TESTUSERLNAME',
        '1234', 'TESTEMAIL@abv.bg', 225, 0),
       (2, 'slav93', 'Biser', 'Slavchev', '1234',
        'slavchev@abv.bg', 1, 1),
       (3, 'anton20', 'Antoni', 'Piryankov', '1234',
        'piryankov@gmail.bg', 0, 1),
       (4, 'victor28', 'Victor', 'Zlatarski', '1234',
        'zlatarski@gmail.bg', 0, 1),
       (5, 'simonaP', 'Simona', 'Peneva', '1234',
        'peneva2@gmail.bg', 152, 0),
       (6, 'penev', 'Dimitur', 'Penev', '1234',
        'penev@gmail.bg', 152, 0);

INSERT INTO `category` (`category_id`, `name`)
VALUES (8, 'ddz sadsdfasda'),
       (9, 'ddz dadada'),
       (10, 'Birds');

INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `phase`, `is_open`, `cover_photo_id`, `created_date_time`,
                        `phase1_expired_time`, `phase2_expired_time`)
VALUES (1, 'The penguin!', 9, 'PHASEONE', 1, NULL, '2023-04-18 12:31:01', '2023-05-03 12:31:01', '2023-05-03 22:31:01'),
       (2, 'PhaseTwoChek', 9, 'FINAL', 1, NULL, '2023-04-16 11:38:29', '2023-04-18 12:36:48', '2023-04-18 13:20:06'),
       (3, 'Birds from Bulgaria', 10, 'PHASETWO', 1, NULL, '2023-04-18 13:21:11', '2023-04-18 13:25:11',
        '2023-05-10 13:21:11');
INSERT INTO `jurors` (`user_id`, `contest_id`, `juror_id`)
VALUES (5, 2, 1),
       (6, 2, 2);

INSERT INTO `photos` (`photo_id`, `title`, `story`, `image`, `author_id`)
VALUES (1, 'The best penguin', 'lorem impsum lorem impsum lorem impsum',
        'photoUpload/TESTUSERNAME/The best penguin.jpg', 1),
       (2, 'Space ', 'lorem impsum lorem impsum lorem impsum', 'photoUpload/slav93/Space .jpg', 2);

INSERT INTO `photos_contests` (`photo_contest_id`, `contest_id`, `photo_id`, `result`)
VALUES (1, 1, 2, 0),
       (2, 2, 1, 10);

INSERT INTO `scores` (`score_id`, `points`, `juror_id`, `comment`, `photo_id`, `contest_id`)
VALUES (1, 10, 5, 'Nice photo from the space! :)', 1, 2);

