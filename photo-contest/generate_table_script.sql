create or replace table category
(
    category_id int auto_increment
        primary key,
    name        varchar(50) not null
);

create or replace table users
(
    user_id     int auto_increment
        primary key,
    username    varchar(20)       not null,
    first_name  varchar(20)       null,
    last_name   varchar(20)       null,
    password    varchar(65)       not null,
    email       varchar(35)       not null,
    points      int     default 0 not null,
    isOrganiser tinyint default 0 not null,
    constraint users_pk2
        unique (username)
);

create or replace table profile_pictures
(
    profile_picture_id int auto_increment
        primary key,
    title              varchar(30)  not null,
    image              varchar(100) not null,
    user_id            int          not null,
    is_active          tinyint      not null,
    constraint profile_pictures_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table photos
(
    photo_id  int auto_increment
        primary key,
    title     varchar(30)  not null,
    story     text         not null,
    image     varchar(100) not null,
    author_id int          not null,
    constraint photos_users_fk
        foreign key (author_id) references users (user_id)
);

create or replace table contests
(
    contest_id          int auto_increment
        primary key,
    title               varchar(30)                                               not null,
    category_id         int                                                       not null,
    phase               enum ('PHASEONE', 'PHASETWO', 'FINAL') default 'PHASEONE' not null,
    is_open             tinyint                                                   not null,
    cover_photo_id      int                                                       null,
    created_date_time   datetime                                                  not null,
    phase1_expired_time datetime                                                  not null,
    phase2_expired_time datetime                                                  not null,
    constraint contests_category_fk
        foreign key (category_id) references category (category_id),
    constraint contests_photos_fk
        foreign key (cover_photo_id) references photos (photo_id)
);

create or replace table jurors
(
    user_id    int not null,
    contest_id int not null,
    juror_id   int auto_increment
        primary key,
    constraint jurors_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint jurors_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table photos_contests
(
    photo_contest_id int auto_increment
        primary key,
    contest_id       int not null,
    photo_id         int not null,
    result           int not null,
    constraint photos_contests_relation_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint photos_contests_relation_photos_fk
        foreign key (photo_id) references photos (photo_id)
);

create or replace table scores
(
    score_id   int auto_increment
        primary key,
    points     int default 0 not null,
    juror_id   int           not null,
    comment    text          not null,
    photo_id   int           null,
    contest_id int           not null,
    constraint scores_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint scores_photos_fk
        foreign key (photo_id) references photos (photo_id),
    constraint scores_user_fk
        foreign key (juror_id) references users (user_id)
);

