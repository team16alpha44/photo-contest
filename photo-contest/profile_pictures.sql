
INSERT INTO `profile_pictures` (`profile_picture_id`, `title`, `image`, `user_id`, `is_active`) VALUES
	(1, 'duck', 'photoUpload/Antoni/profilePictures/duck.jpg', 1, 1),
	(2, 'boxhead', 'photoUpload/Antoni/profilePictures/boxhead.jpg', 1, 0),
	(3, 'lizard-king', 'photoUpload/Biser/profilePictures/lizard-king.jpg', 2, 1),
	(4, 'monkey', 'photoUpload/Victor/profilePictures/monkey.jpg', 3, 1),
	(5, 'myphoto', 'photoUpload/sportsfanatic12/profilePictures/myphoto.jpg', 4, 1),
	(6, 'emily-profile', 'photoUpload/creative_soul/profilePictures/emily-profile.jpg', 5, 1),
	(7, 'gasmask', 'photoUpload/technerd88/profilePictures/gasmask.jpg', 6, 1),
	(8, 'with-beard', 'photoUpload/fitnessguru/profilePictures/with-beard.jpg', 8, 1),
	(9, 'red-bricks', 'photoUpload/musiclover22/profilePictures/red-bricks.jpg', 9, 1),
	(10, 'my-dog', 'photoUpload/gamerzone7/profilePictures/my-dog.jpg', 10, 1),
	(11, 'succulent', 'photoUpload/travelholic99/profilePictures/succulent.jpg', 11, 1),
	(12, 'kebabb', 'photoUpload/foodiegirl/profilePictures/kebabb.jpg', 12, 1),
	(13, 'selfiee', 'photoUpload/naturelover77/profilePictures/selfiee.jpg', 13, 1),
	(14, 'pretty', 'photoUpload/bookworm101/profilePictures/pretty.jpg', 7, 1);
