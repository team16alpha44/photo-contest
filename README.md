## Project Description

A team of aspiring photographers want an application that can allow them to easily manage online photo contests.
The application has two main parts:

- Organizational – here, the application owners can organize photo contests.
- For photo junkies – everyone is welcome to register and to participate in contests. Junkies with certain ranking can be invited to be jury.

## Functional Requirements

- Public Part

The public part is accessible without authentication i.e. for anonymous users.

* Landing page – you can show the latest winning photos or something else that might be compelling for people to register.

* Login form – redirects to the private area of the application. Login requires username and password.

* Register form – registers someone as a Photo Junkie. Requires username, email, first name, last name, and password.

- Private part

Accessible only if the user is authenticated.

* Dashboard page

Dashboard page is different for Organizers and Photo Junkies

* For Organizers.

o	There is a way to setup a new Contest.

o	There is a way to view Contests which are in Phase I.

o	There is a way to view Contests which are in Phase II.

o	There is a way to view Contests which are Finished.

o	Can make other users organizers.

o	Is able to delete contest entry (due to offensive nature of the photo, comment etc.) Jury is able to delete as well.

o	Jury is not be able to participate within the same contest where they are jury.

o	There is a way to view Photo Junkies.

* For Photo Junkies:

o	There is a way to view active Open contests.

o	There is a way to view contests that the junkie currently participates in.

o	There is a way to view finished contests that the junkie participated in.

o	Display current points and ranking and how much until next ranking at a visible place

* Contest page

The Contest Category is always visible.

-	Phase I

•	Remaining time until Phase II is displayed.

•	Jury can view submitted photos but cannot rate them yet.

•	Junkies see enroll button if the contest is Open and they are not participating.

•	If they are participating and have not uploaded a photo, they see a form for upload:

1.	Title – short text
2.	Story – long text, which tells the captivating story of the phot
3.	Photo – file

•	Only one photo can be uploaded per participant. The photo, title, and story cannot be edited. Display a warning on submit that any data cannot be changed later

-	Phase II

•	Remaining time until Finish phase.

•	Participants cannot upload anymore.

•	Jury sees a form for each submitted photo. 

	Score (1-10)
	Comment (long text)
	Checkbox to mark that the photo does not fit the contest category. If the checkbox is selected, score 0 is assigned automatically and a Comment that the category is wrong. This is the only way to assign Score outside the [1, 10] interval.
	Each juror can give one review per photo, if a photo is not reviewed, a default score of 3 is awarded.

-	Finished

•	Jury can no longer review photos.

•	Participants view their score and comments.

•	In this phase, participants can also view the photos submitted by other users, along with their scores and comments by the Jury.

* Create Contest Page

o	Title – text field

o	Category – text field

o	Open or Invitational Contest. Open means that everyone (except the jury can join)

o	Phase I time limit – anything from one day to one month

o	Phase II time limit – anything from one hour to one day 

o	Select Jury – all users with Organizer role are automatically selected

•	Additionally, users with ranking Photo Master can also be selected, if the organizers decide

o	Cover photo - a photo can be selected for a contest.

•	upload a cover photo.

•	The organizer is able to choose between all three options and select the easiest for him/her

## Scoring

Contest participation awards points. Points are accumulative, so being invited and subsequently winning will award 53 points totals.

o	Joining open contest – 1 point

o	Being invited by organizer – 3 points

o	3rd place – 20 points (10 points if shared 3rd)

o	2nd place – 35 points (25 points if shared 2nd)

o	1st place – 50 points (40 points if shared 1st)

o	Finishing at 1st place with double the score of the 2nd (e.g., 1st has been awarded 8.6 points average, and 2nd is 4.3 or less) – 75 points

o	In case of a tie, positions are shared, so there can be more than one participant at 1st, 2nd, and 3rd places, all in the same contest.

For example, two 1st places, one 2nd and four 3rds; the two winners will each get 40 points, the only 2nd place will get the full 35 points, and the four 3rd finishers will get 10 points.

## Ranking

o	(0-50) points – Junkie

o	(51 – 150) points – Enthusiast

o	(151 – 1000) points – Master (can now be invited as jury)

o	(1001 – infinity) points – Wise and Benevolent Photo Dictator (can still be jury)

## Social Sharing

Participants have to option to share their achievement to a social media.


## Swagger documentation:

https://app.swaggerhub.com/apis/VICTORZLATARSKI_1/PhotoArenaDocumentation/v1

## REST API

To provide other developers with your service, you need to develop a REST API. It leverages HTTP as a transport protocol and clear text JSON for the request and response payloads.
The REST API provides the following capabilities:
1.	Users

•	CRUD Operations

•	List and search by username, first name or last name

2.	Contests

•	CRUD Operations

•	Submit photo

•	Rate photo

•	List and filter by title, category, type and phase

3.	Photos

•	CRD Operations

•	List and search by title


## Database

The data of the application is stored in a relational database.